<div x-data="{open:false, option:-1, sold:0}" class="flex flex-col sm:flex-row">
  @can('delete', $ad)
  <a
    href="#"
    @click.prevent="open = true; sold = 1; option=-1"
    class="cursor-pointer bg-green-600 hover:bg-green-700 text-white text-center font-bold py-2 px-4 mt-4 sm:mt-0 sm:ml-4 rounded-lg"
  ><i class="icon-thumbs-up mr-2"></i>Já vendi!</a>
  {!! Form::model($ad, ["route" => ["ads.destroy", $ad], "method" => "delete", "x-ref" => "form"]) !!}
  {!! Form::hidden('sold', null, ["x-model"=>"option"]) !!}
  {!! Form::button('<i class="icon-trash mr-2"></i> Excluir', [
    "class" => "cursor-pointer bg-red-400 hover:bg-red-500 text-center text-white font-bold py-2 px-4 w-full sm:w-auto mt-4 sm:mt-0 sm:ml-4 rounded-lg",
    "@click.prevent"=>"open = true; sold = 0;option=-1"
  ]) !!}
  {!! Form::close() !!}
  @endcan
<div x-show="open" class="fixed inset-0 bg-opacity-50 bg-black flex justify-center items-center" style="display:none">
  <div class="bg-white rounded-lg p-4 w-5/6 sm:w-3/5 relative shadow-2xl" @click.away="open=false">
    <i class="icon-cancel absolute right-0 top-0 cursor-pointer m-4" @click="open=false"></i>
    <h2 class="text-2xl font-bold text-gray-800 mb-6">Excluir anúncio</h2>
    <div x-show="sold == 0" class="mb-6">
      Antes de excluir seu anúncio, conta para a gente: conseguiu vender este item?
    </div>
    <div x-show="sold == 1" class="mb-6">
      Que ótimo que você conseguiu vender! Conta para a gente: por onde você vendeu?
    </div>
    <div class="cursor-pointer mb-4" @click="option = 1">
      <i class="icon-circle text-gray-600 text-xl" x-show="option != 1"></i>
      <i class="icon-ok-circled text-green-600 text-xl" x-show="option == 1"></i>
      <span class="ml-2">Vendi pelo <span class="font-bold text-teal-600">Vapt!</span></span>
    </div>
    <div class="cursor-pointer mb-4" @click="option = 2">
      <i class="icon-circle text-gray-600 text-xl" x-show="option != 2"></i>
      <i class="icon-ok-circled text-green-600 text-xl" x-show="option == 2"></i>
      <span class="ml-2">Vendi por outro lugar</span>
    </div>
    <div x-show="sold == 0" class="cursor-pointer mb-4" @click="option = 3">
      <i class="icon-circle text-gray-600 text-xl" x-show="option != 3"></i>
      <i class="icon-ok-circled text-green-600 text-xl" x-show="option == 3"></i>
      <span class="ml-2">Ainda não vendi</span>
    </div>
    <div class="flex flex-col sm:flex-row sm:justify-end">
    <a
      href="#"
      @click.prevent="open=false"
      class="hover:underline text-gray-800 font-bold py-2 px-4 text-center"
    >Cancelar</a>
    <a
      href="#"
      :disabled="option == -1"
      @click.prevent="$refs.form.submit()"
      class="bg-teal-600 hover:bg-teal-700 text-white font-bold py-2 text-center px-4 sm:ml-4 rounded-lg"
    >Excluir</a>
    </div>
  </div>
</div>
</div>
