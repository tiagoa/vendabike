@extends("layouts.app")
@section("content")
<div class="flex flex-col-reverse md:flex-row">
  <div class="bg-white rounded px-8 pt-6 pb-8 w-full md:w-3/4">
    {!! Form::model($user, ["route" => ["users.update-password"], "method" => "put"]) !!}
    <h2 class="text-2xl font-bold text-gray-500 mb-6">Alterar senha</h2>
    <div class="mb-4 flex flex-col">
      {!! Form::label("old_password", "Senha atual") !!}
      {!! Form::password("old_password", ["class" => "border-2 rounded py-2 px-4", "required" => true]) !!}
      @error("old_password")
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
    <div class="mb-4 flex flex-col">
      {!! Form::label("new_password", "Nova senha") !!}
      {!! Form::password("new_password", ["class" => "border-2 rounded py-2 px-4", "required" => true]) !!}
      @error("new_password")
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
    <div class="mb-8 flex flex-col">
      {!! Form::label("confirm_new_password", "Confirme a nova senha") !!}
      {!! Form::password("confirm_new_password", ["class" => "border-2 rounded py-2 px-4", "required" => true]) !!}
      @error("confirm_new_password")
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
    {!! Form::submit("Atualizar", ["class" => "bg-teal-600 hover:bg-teal-700 text-white font-bold py-2 px-4 rounded"]) !!}
    {!! Form::close() !!}
  </div>
  <x-menu-profile />
</div>
@endsection()
