<?php

namespace App;

use App\Enums\ContactTypeEnum;
use Illuminate\Database\Eloquent\Model;

class UserContact extends Model
{

    protected $fillable = ["type", "value"];

    public function getUrlAttribute()
    {
        if ($this->attributes['type'] == ContactTypeEnum::INSTAGRAM) {
            return 'https://instagram.com/'.$this->attributes['value'];
        }
        if ($this->attributes['type'] == ContactTypeEnum::WHATSAPP) {
            $url = 'https://wa.me/55'.$this->attributes['value'].'/?text=';
            $url .= urlencode("Olá! Me interessei pelo seu anúncio no Vapt!");
            return $url;
        }
        if ($this->attributes['type'] == ContactTypeEnum::FACEBOOK) {
            return $this->attributes['value'];
        }
        if ($this->attributes['type'] == ContactTypeEnum::GOOGLE) {
            return 'mailto:'.$this->attributes['value'];
        }
        return '#';
    }

    public function getNameAttribute()
    {
        return ContactTypeEnum::$name[$this->attributes['type']];
    }

    public function getSlugAttribute()
    {
        return ContactTypeEnum::$slug[$this->attributes['type']];
    }

}
