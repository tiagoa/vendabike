@extends("layouts.app")
@section("content")
<div class="flex flex-col-reverse md:flex-row">
  <div class="bg-white rounded px-8 pt-6 pb-8 w-full md:w-3/4">
    {!! Form::model($user, ["route" => ["users.update-contacts"], "method" => "put"]) !!}
    <h2 class="text-2xl font-bold text-gray-500 mb-6">Formas de contato</h2>
    @foreach ($user->contacts as $contact)
    <div class="flex flex-col mb-4">
      {!! Form::label("contacts[".$contact->type."][value]", $contact->name) !!}
      {!! Form::text("contacts[".$contact->type."][value]", null, ["class" => "border-2 rounded py-2 px-4"]) !!}
      {!! Form::hidden("contacts[".$contact->type."][type]", null) !!}
      {!! Form::hidden("contacts[".$contact->type."][id]", null) !!}
      @error("user.contacts[".$contact->type."].value")
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
    @endforeach
    {!! Form::submit("Atualizar", ["class" => "bg-teal-600 hover:bg-teal-700 text-white font-bold py-2 px-4 rounded"]) !!}
    {!! Form::close() !!}
  </div>
  <x-menu-profile />
</div>
@endsection()
