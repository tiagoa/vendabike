<?php
namespace App\Services;

use Image;

class InstagramBot
{

    public static function start()
    {
        $instagram = new \InstagramScraper\Instagram();
        $instagram->setRapidApiKey(env("RAPID_API_KEY"));
        return $instagram;
    }

    public static function getPost($url)
    {
        $instagram = self::start();
        return $instagram->getMediaByUrl($url);
    }

    public static function getProfilePictureFromInstagramByUsername($username)
    {
        $instagram = self::start();
        $account = $instagram->getAccountInfo($username);

        $path = public_path().DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR."users".DIRECTORY_SEPARATOR;

        $download = $account->getProfilePicUrl();
        $filename = md5(microtime());
        $filepath = $path.$filename;
        $webp = Image::make($download)->encode("webp", 90);
        $webp->save($filepath.".webp");
        $jpeg = Image::make($download)->encode("jpeg", 90);
        $jpeg->save($filepath.".jpeg");

        return route('home').'/uploads/users/'.$filename;
    }
}
