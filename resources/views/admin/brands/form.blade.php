@extends('layouts.admin')
@section('content')
<div class="bg-white rounded-lg p-8">
  @if ($brand)
  {!! Form::model($brand, ['route' => ['admin.brands.update', $brand->id], "class"=>"px-4 sm:px-0", 'method' => 'put']) !!}
    <h2 class="font-bold text-2xl text-blue-800 mb-4">Atualizar marca</h2>
    {!! Form::hidden('id', $brand->id) !!}
  @else
  {!! Form::open(['route' => 'admin.brands.store', "class"=>"px-4 sm:px-0"]) !!}
    <h2 class="font-bold text-2xl text-blue-800 mb-4">Cadastrar marca</h2>
  @endif
  <div class="mb-8 flex flex-col sm:w-1/2">
    {!! Form::label('name', 'Nome') !!}
    {!! Form::text('name', null, ['class' => 'border-2 rounded py-2 px-4']) !!}
    @error('name')
      <div class="text-red-500">{{ $message }}</div>
    @enderror
  </div>
  <div>
    <button class="bg-teal-600 hover:bg-teal-700 text-white font-bold py-2 px-4 rounded-lg">Salvar</button>
    <a
      href="{{ route("admin.brands.index")}}"
      class="hover:underline text-gray-800 font-bold py-2 px-4 text-center"
    >Cancelar</a>
  </div>
{!! Form::close() !!}
</div>
@endsection
