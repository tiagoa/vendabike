<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Repositories\AdRepository;
use Illuminate\Http\Request;

class Home extends Controller
{
    protected $ad;

    public function __construct(AdRepository $ad)
    {
        $this->ad = $ad;
    }

    public function __invoke(Request $request)
    {
        $ads = $this->ad->forHomePage();
        $bikeByTypeAndBrand = $this->ad->bikeByTypeAndBrand();
        return view("home", compact("ads", "bikeByTypeAndBrand"));
    }
}
