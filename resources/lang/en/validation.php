<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'Campo :attribute deve ser aceito.',
    'active_url' => 'Campo :attribute não é uma URL válida.',
    'after' => 'Campo :attribute deve ser depois de :date.',
    'after_or_equal' => 'Campo :attribute deve ser depois ou igual a :date.',
    'alpha' => 'Campo :attribute só pode conter letras.',
    'alpha_dash' => 'Campo :attribute só pode conter letras, números, traços e underlines.',
    'alpha_num' => 'Campo :attribute só pode conter letras e números.',
    'array' => 'Campo :attribute deve ser uma lista.',
    'before' => 'Campo :attribute deve ser antes de :date.',
    'before_or_equal' => 'Campo :attribute deve ser antes ou igual a :date.',
    'between' => [
        'numeric' => 'Campo :attribute deve estar entre :min e :max.',
        'file' => 'Campo :attribute deve estar entre :min e :max kilobytes.',
        'string' => 'Campo :attribute deve estar entre :min e :max caracteres.',
        'array' => 'Campo :attribute deve conter de :min a :max itens.',
    ],
    'boolean' => 'Campo :attribute deve ser verdadeiro ou falso.',
    'confirmed' => 'Campo :attribute e a confirmação não são iguais.',
    'date' => 'Campo :attribute não é uma data válida.',
    'date_equals' => 'Campo :attribute deve ser igual a :date.',
    'date_format' => 'Campo :attribute não está no formato :format.',
    'different' => 'Campo :attribute e :other deve ser diferentes.',
    'digits' => 'Campo :attribute só pode conter :digits dígitos.',
    'digits_between' => 'Campo :attribute deve estar entre :min e :max dígitos.',
    'dimensions' => 'Campo :attribute contém dimensões inválidas.',
    'distinct' => 'Campo :attribute já existe.',
    'email' => 'Campo :attribute deve ser um e-mail válido.',
    'ends_with' => 'Campo :attribute só pode terminar com os seguintes valores: :values.',
    'exists' => ':attribute selecionado é inválido.',
    'file' => 'Campo :attribute deve ser um arquivo.',
    'filled' => 'Campo :attribute deve ser preenchido.',
    'gt' => [
        'numeric' => 'Campo :attribute deve ter mais que :value.',
        'file' => 'Campo :attribute deve ter mais que :value kilobytes.',
        'string' => 'Campo :attribute deve ter mais que :value caracteres.',
        'array' => 'Campo :attribute deve ter mais que :value itens.',
    ],
    'gte' => [
        'numeric' => 'Campo :attribute deve ser maior ou igual a :value.',
        'file' => 'Campo :attribute deve ser maior ou igual a :value kilobytes.',
        'string' => 'Campo :attribute deve ser maior ou igual a :value caracteres.',
        'array' => 'Campo :attribute deve ter :value itens ou mais.',
    ],
    'image' => 'Campo :attribute deve ser uma imagem.',
    'in' => 'Valor para :attribute é inválido.',
    'in_array' => 'Campo :attribute não existe em :other.',
    'integer' => 'Campo :attribute deve ser um número inteiro.',
    'ip' => 'Campo :attribute deve ser um IP válido.',
    'ipv4' => 'Campo :attribute deve ser um IPv4 válido.',
    'ipv6' => 'Campo :attribute deve ser um IPv6 válido.',
    'json' => 'Campo :attribute deve ser um JSON válido.',
    'lt' => [
        'numeric' => 'Campo :attribute deve ser menor que :value.',
        'file' => 'Campo :attribute deve ser menor que :value kilobytes.',
        'string' => 'Campo :attribute deve ser menor que :value caracteres.',
        'array' => 'Campo :attribute deve ter menos que :value itens.',
    ],
    'lte' => [
        'numeric' => 'Campo :attribute deve ter :value ou menos.',
        'file' => 'Campo :attribute deve ter :value kilobytes ou menos.',
        'string' => 'Campo :attribute deve ter :value caracteres ou menos.',
        'array' => 'Campo :attribute não pode ter mais do que :value itens.',
    ],
    'max' => [
        'numeric' => 'Campo :attribute não pode ter mais que :max.',
        'file' => 'Campo :attribute não pode ter mais que :max kilobytes.',
        'string' => 'Campo :attribute não pode ter mais que :max caracteres.',
        'array' => 'Campo :attribute não pode ter mais que :max itens.',
    ],
    'mimes' => 'Campo :attribute deve ser do tipo: :values.',
    'mimetypes' => 'Campo :attribute deve ser do tipo: :values.',
    'min' => [
        'numeric' => 'Campo :attribute deve ter no mínimo :min.',
        'file' => 'Campo :attribute deve ter no mínimo :min kilobytes.',
        'string' => 'Campo :attribute deve ter no mínimo :min caracteres.',
        'array' => 'Campo :attribute deve ter no mínimo :min itens.',
    ],
    'not_in' => 'Valor para :attribute é inválido.',
    'not_regex' => 'Foramato do campo :attribute é inválido.',
    'numeric' => 'Campo :attribute deve ser um número.',
    'password' => 'Senha incorreta.',
    'present' => 'Campo :attribute deve estar presente.',
    'regex' => 'Formato do campo :attribute é inválido.',
    'required' => 'Campo :attribute é obrigatório.',
    'required_if' => 'Campo :attribute é obrigatório quando :other for :value.',
    'required_unless' => 'Campo :attribute é obrigatório a menos que :other for :values.',
    'required_with' => 'Campo :attribute é obrigatório quando tiver :values.',
    'required_with_all' => 'Campo :attribute é obrigatório quando tiver :values.',
    'required_without' => 'Campo :attribute é obrigatório quando não tiver :values.',
    'required_without_all' => 'Campo :attribute é obrigatório quando não tiver :values.',
    'same' => 'Campo :attribute e :other devem ser iguais.',
    'size' => [
        'numeric' => 'Campo :attribute deve ter :size.',
        'file' => 'Campo :attribute deve ter :size kilobytes.',
        'string' => 'Campo :attribute deve ter :size caracteres.',
        'array' => 'Campo :attribute must contain :size itens.',
    ],
    'starts_with' => 'Campo :attribute deve começar com :values.',
    'string' => 'Campo :attribute deve ser um texto.',
    'timezone' => 'Campo :attribute deve ser um fuso-horário.',
    'unique' => 'Este :attribute já existe.',
    'uploaded' => 'Falha no carregamento do campo :attribute.',
    'url' => 'Formato do campo :attribute é inválido.',
    'uuid' => 'Campo :attribute deve ser um UUID válido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    "attributes" => [
        "bicycle_type_id" => "tipo de bike",
        "brand_id" => "marca",
        "category_id" => "categoria",
        "city_id" => "cidade",
        "confirm_new_password" => "confirmação de nova senha",
        "description" => "descrição",
        "details" => "detalhes",
        "email" => "e-mail",
        "name" => "nome",
        "new_password" => "nova senha",
        "old_password" => "senha atual",
        "password" => "senha",
        "password_confirmation" => "confirmação de senha",
        "photos" => "fotos",
        "price" => "preço",
        "sold" => "motivo de exclusão",
        "usage_time" => "tempo de uso",
        "zipcode" => "CEP",
    ],

];
