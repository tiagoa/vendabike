<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->id();
            $table->foreignId('brand_id')->nullable()->constrained();
            $table->foreignId('category_id')->constrained();
            $table->foreignId('city_id')->nullable()->constrained();
            $table->foreignId('user_id')->constrained();
            $table->decimal('price', 8, 2);
            $table->string('description', 255);
            $table->text('details');
            $table->tinyInteger('receipt')->default(0);
            $table->tinyInteger('status');
            $table->tinyInteger('deletion_reason')->nullable();
            $table->string('usage_time', 100)->nullable();
            $table->integer('views')->default(0);
            $table->tinyInteger('negotiation')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads');
    }
}
