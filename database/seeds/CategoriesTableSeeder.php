<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->delete();
        Category::create(['name' => 'Bikes', 'slug' => 'bikes']);
        Category::create(['name' => 'Acessórios', 'slug' => 'acessorios']);
        Category::create(['name' => 'Componentes', 'slug' => 'componentes']);
        Category::create(['name' => 'Vestuário', 'slug' => 'vestuarios']);
    }
}
