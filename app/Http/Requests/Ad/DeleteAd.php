<?php

namespace App\Http\Requests\Ad;

use App\Ad;
use Illuminate\Foundation\Http\FormRequest;

class DeleteAd extends FormRequest
{
    public function authorize()
    {
        $ad = Ad::find($this->route("ad")->id);
        return $this->user()->can("delete", $ad);
    }

    public function rules()
    {
        return [
            'sold' => 'required|in:1,2,3',
        ];
    }
}
