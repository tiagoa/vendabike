<?php

namespace App;

use App\Enums\AdStatusEnum;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Ad extends Model
{
    use SoftDeletes;

    protected $with = ["brand"];

    public function scopePublished($query)
    {
        return $query->where('status', AdStatusEnum::PUBLISHED);
    }

    public function bike()
    {
        return $this->hasOne("App\Bicycle");
    }

    public function comments()
    {
        return $this->hasMany("App\Comment");
    }

    public function brand()
    {
        return $this->belongsTo("App\Brand");
    }

    public function category()
    {
        return $this->belongsTo("App\Category");
    }

    public function photos()
    {
        return $this->hasMany("App\Photo");
    }

    public static function forHomePage()
    {
        return self::whereStatus(1);
    }

    public function user()
    {
        return $this->belongsTo("App\User");
    }

    public function getSlugAttribute()
    {
        $slug = [];
        if ($this->bike) {
            $slug[] = $this->bike->type->name;
        }
        if ($this->brand) {
            $slug[] = $this->brand->slug;
        }
        $slug[] = $this->attributes["description"];
        return Str::slug(implode(" ", $slug));
    }

    public function getWasSoldAttribute()
    {
        return $this->attributes["status"] == AdStatusEnum::SOLD;
    }

    public function getCityAttribute()
    {
        if ($this->user && $this->user->city) {
            return $this->user->city->name . ' - ' . $this->user->city->state_uf;
        }
        return '';
    }

    public function setPriceAttribute($value)
    {
        $this->attributes["price"] = float_from_brl($value);
    }

    public function getStatusAttribute()
    {
        return AdStatusEnum::$name[$this->attributes["status"]];
    }


}
