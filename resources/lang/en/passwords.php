<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    "reset" => "Sua senha foi alterada!",
    "sent" => "Enviamos um email com um link para alterar sua senha!",
    "throttled" => "Espere um pouco antes de tentar novamente, por favor.",
    "token" => "Esse link de alterar a senha é inválido.",
    "user" => "Não existe cadastro com esse e-mail."

];
