<?php

namespace App\Http\Requests\Ad;

use Illuminate\Foundation\Http\FormRequest;

class NewAd extends FormRequest
{
    public function authorize()
    {
        return $this->user() ? true : false;
        // return true;
    }

    public function rules()
    {
        return [
            'bicycle_type_id' => 'requiredIf:category_id,1|integer|exists:bicycle_types,id',
            'brand' => 'required',
            'brand_id' => 'nullable|integer|exists:brands,id',
            'source' => 'nullable|unique:ads,source',
            'description' => 'required|min:10',
            'details' => 'required|min:10',
            'category_id' => 'required|integer|exists:categories,id',
            'photos' => 'array|max:6',
            'photos.*' => 'mimes:jpeg,png|max:1024',
            'price' => 'required|regex:/^(\d{1,3}\.)?(\d{1,3}\.)?\d{1,3},\d{2}$/'
        ];
    }
}
