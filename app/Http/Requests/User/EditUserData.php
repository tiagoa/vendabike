<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class EditUserData extends FormRequest
{
    public function authorize()
    {
        return $this->user()->can("update", $this->user());
    }

    public function rules()
    {
        return [
            'email' => 'required_with:email_required|email|unique:users,email',
            "city_id" => "required|exists:cities,id",
            "zipcode" => "required",
            "name" => "required|min:3",
        ];
    }

    public function messages()
    {
        return [
            'email.required_with' => "E-mail é obrigatório"
        ];
    }
}
