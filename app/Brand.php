<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    public function ads()
    {
        return $this->hasMany("App\Ad");
    }
}
