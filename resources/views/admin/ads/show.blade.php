@extends('layouts.admin')
@section('content')
<div class="flex flex-col -mt-8 sm:mt-0 md:flex-row">
  <div class="w-full md:w-3/4 md:pr-8">
    <div class="flex flex-row justify-center bg-gray-800">
    @foreach ($ad->photos as $photo)
      <div
        class="w-1/6 bg-cover bg-center bg-no-repeat cursor-pointer transition duration-300 hover:opacity-75"
        style="padding-top:10%; background-image:url('{{photo_thumb($photo->file)}}')"
      >
      </div>
    @endforeach()
    </div>
    <div class="bg-white p-4 pb-0">
      <div>
        <div class="float-right text-right ml-2 mr-1">
          @if ($ad->city)
          <div class="mb-2">
            <i class="icon-location text-gray-600"></i> {{$ad->city}}
          </div>
          @endif
          @if ($ad->receipt)
          <div class="mb-2">
            <div class="receipt">
            <i class="icon-file-alt text-gray-600"></i> Nota fiscal
            </div>
          </div>
          @endif
          @if ($ad->negotiation)
          <div class="receipt">
            <i class="icon-handshake text-gray-600"></i> Aceita negociar
          </div>
          @endif
        </div>
        <h2 class="font-bold text-2xl pb-2 text-green-600">{{ brl_without_zero($ad->price) }}</h2>
        <h2 class="font-bold text-2xl pb-3">{{ $ad->description }}</h2>
      </div>
      <div class="pb-4">{!! nl2br($ad->details, 0) !!}</div>
      <div class="flex flex-col sm:flex-row -mx-4 border-gray-200 border-t">
        <div class="text-gray-700 font-bold w-full pl-4 pt-4 sm:py-4 sm:w-1/3 mr-2">Marca</div>
        <div>
          <a
            href="{{ route("ads.brand", ["brand"=>$ad->brand->slug]) }}"
            title="Ver mais bicicletas da marca {{$ad->brand->name}}"
            class="text-blue-500 inline-block py-2 px-4 mb-2 sm:mb-0 sm:mt-2 sm:-ml-2 rounded-full hover:text-white hover:bg-blue-500 transition duration-300"
          >{{$ad->brand->name}}</a>
        </div>
      </div>
      @if($ad->bike)
      @if ($ad->bike->frame_size)
      <div class="flex flex-col sm:flex-row -mx-4 p-4 border-gray-200 border-t">
        <div class="text-gray-700 font-bold w-full sm:w-1/3 mr-2">Quadro / Tamanho</div><div>{{$ad->bike->frame_size}}</div>
      </div>
      @endif
      @if ($ad->bike->fork_headset)
      <div class="flex flex-col sm:flex-row -mx-4 p-4 border-gray-200 border-t">
        <div class="text-gray-700 font-bold w-full sm:w-1/3 mr-2">Garfo / Headset</div><div>{{$ad->bike->fork_headset}}</div>
      </div>
      @endif
      @if ($ad->bike->handlebars_stem)
      <div class="flex flex-col sm:flex-row -mx-4 p-4 border-gray-200 border-t">
        <div class="text-gray-700 font-bold w-full sm:w-1/3 mr-2">Guidão / Mesinha</div><div>{{$ad->bike->handlebars_stem}}</div>
      </div>
      @endif
      @if ($ad->bike->groupset)
      <div class="flex flex-col sm:flex-row -mx-4 p-4 border-gray-200 border-t">
        <div class="text-gray-700 font-bold w-full sm:w-1/3 mr-2">Grupo</div><div>{{$ad->bike->groupset}}</div>
      </div>
      @endif
      @if ($ad->bike->brakes)
      <div class="flex flex-col sm:flex-row -mx-4 p-4 border-gray-200 border-t">
        <div class="text-gray-700 font-bold w-full sm:w-1/3 mr-2">Freios</div><div>{{$ad->bike->brakes}}</div>
      </div>
      @endif
      @if ($ad->bike->pedals)
      <div class="flex flex-col sm:flex-row -mx-4 p-4 border-gray-200 border-t">
        <div class="text-gray-700 font-bold w-full sm:w-1/3 mr-2">Pedais</div><div>{{$ad->bike->pedals}}</div>
      </div>
      @endif
      @if ($ad->bike->saddle_seatpost)
      <div class="flex flex-col sm:flex-row -mx-4 p-4 border-gray-200 border-t">
        <div class="text-gray-700 font-bold w-full sm:w-1/3 mr-2">Selim / Canote</div><div>{{$ad->bike->saddle_seatpost}}</div>
      </div>
      @endif
      @if ($ad->bike->front_wheel_hub_tire)
      <div class="flex flex-col sm:flex-row -mx-4 p-4 border-gray-200 border-t">
        <div class="text-gray-700 font-bold w-full sm:w-1/3 mr-2">Roda / Cubo / Pneu dianteiro</div><div>{{$ad->bike->front_wheel_hub_tire}}</div>
      </div>
      @endif
      @if ($ad->bike->rear_wheel_hub_tire)
      <div class="flex flex-col sm:flex-row -mx-4 p-4 border-gray-200 border-t">
        <div class="text-gray-700 font-bold w-full sm:w-1/3 mr-2">Roda / Cubo / Pneu traseiro</div><div>{{$ad->bike->rear_wheel_hub_tire}}</div>
      </div>
      @endif
      @endif
    </div>
    <div class="bg-white border-t border-gray-200 p-4">
      <div class="flex flex-col sm:flex-row">
        <a href="{{ route('ads.edit', ['ad' => $ad->id]) }}" class="inline-block bg-blue-400 hover:bg-blue-500 text-white font-bold py-2 px-4 rounded-lg text-center">Atualizar</a>
        {!! Form::model($ad, ["route" => ["admin.ads.destroy", $ad], "method" => "delete"]) !!}
        {!! Form::select('sold', $deletionReasons) !!}
        {!! Form::button('<i class="icon-trash mr-2"></i> Excluir', [
          "class" => "cursor-pointer bg-red-400 hover:bg-red-500 text-center text-white font-bold py-2 px-4 w-full sm:w-auto mt-4 sm:mt-0 sm:ml-4 rounded-lg", "type"=>"submit"]) !!}
        {!! Form::close() !!}
      </div>
      <x-user :user="$ad->user" />
    </div>
  </div>
</div>
@endsection
