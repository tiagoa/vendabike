@extends('layouts.app')
@section('content')
<h2 class="text-2xl font-bold text-blue-800 mb-6 text-center sm:text-left">Bicicletas {{ $bikeType->name }} {{ $brand->name }} à venda</h2>
@if ($ads->count())
<x-ad-grid :ads="$ads" />
@else
<div class="bg-blue-200 text-center rounded-lg p-8">Por enquanto não tem nenhum anúncio de bicicleta {{ $bikeType->name }} {{$brand->name}} disponível para venda.</div>
@endif
<x-type-brand :bikeByTypeAndBrand="$bikeByTypeAndBrand" />
@endsection
