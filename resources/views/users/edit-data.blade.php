@extends("layouts.edit-user")
@section("content")
<div class="flex flex-col-reverse md:flex-row">
  <div class="bg-white rounded px-8 pt-6 pb-8 w-full md:w-3/4">
    {!! Form::model($user, ["route" => ["users.update-data"], "method" => "put"]) !!}
    <h2 class="text-2xl font-bold text-gray-500 mb-6">Meus dados</h2>
    <div class="mb-4 flex flex-col">
      {!! Form::label("name", "Nome *") !!}
      {!! Form::text("name", null, ["class" => "border-2 rounded py-2 px-4", "required" => true]) !!}
      @error("name")
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
    <div class="mb-4 flex flex-col">
      @if($user->email == null)
      {!! Form::label("email", "E-mail *") !!}
      {!! Form::hidden("email_required", 1) !!}
      {!! Form::email("email", null, ["class" => "border-2 rounded py-2 px-4"]) !!}
      @else
      {!! Form::label("email", "E-mail") !!}
      {!! Form::email("email", null, ["class" => "border-2 rounded py-2 px-4", "disabled" => true]) !!}
      @endif
    </div>
    <x-user-zipcode-city :user="$user" />
    {!! Form::submit("Atualizar", [":disabled"=>"loading","class" => "w-full sm:w-auto bg-teal-600 hover:bg-teal-700 text-white font-bold py-2 px-4 rounded"]) !!}
    {!! Form::close() !!}
  </div>
  <x-menu-profile />
</div>
@endsection()
