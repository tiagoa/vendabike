@extends('layouts.admin')
@section('content')
<div class="bg-white rounded-lg p-8">
  <a href="{{ route("admin.users.create") }}" class="inline-block bg-blue-400 hover:bg-blue-500 text-white font-bold py-2 px-4 rounded-lg text-center">Cadastrar usuário</a>
  <h2 class="mt-6 text-2xl font-bold text-gray-500 mb-6">{{ $users->count() }} Usuários</h2>
  <table class="w-full bg-white">
    <thead>
      <tr class="text-left">
        <th class="text-center">#</th>
        <th colspan="2">Nome</th>
        <th>Anúncios</th>
        <th>Cadastrado em</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach ($users as $user)
      <tr class="bg-{{ $loop->index % 2 == 0 ? "white" : "gray-200" }}">
        <td class="text-center">{{ $user->id }}</td>
        <td>
          @if ($user->avatar)
            <span
            class="inline-block w-8 h-8 bg-no-repeat bg-center bg-contain"
            style="background-image:url('{{ $user->avatar }}.webp')"
            ></span>
          @else
            <span
            class="inline-block w-8 h-8 bg-no-repeat bg-center bg-contain"
            style="background-image:url('{{ gravatar($user->email) }}')"
            ></span>
          @endif
        </td>
        <td>{{ $user->name }}</td>
        <td>{{ $user->ads_count }}</td>
        <td>{{ $user->created_at }}</td>
        <td>
          <div class="opacity-0 hover:opacity-100 flex flex row">
            <a href="{{ route("admin.users.edit", ["user"=>$user]) }}">editar</a>
            {!! Form::model($user, ["route"=>["admin.users.destroy", "user"=>$user], "method"=>"delete"]) !!}
            <input type="submit" value="deletar" class="bg-transparent ml-2">
            {!! Form::close() !!}
          </div>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
@endsection
