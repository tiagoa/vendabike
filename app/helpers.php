<?php
if (! function_exists(('webp_or_jpeg'))) {
    function webp_or_jpeg() {
        if (!isset($_SERVER["HTTP_ACCEPT"])) {
            return ".jpeg";
        }
        return preg_match("/image\/webp/i", $_SERVER['HTTP_ACCEPT']) ? ".webp" : ".jpeg";
    }
}
if (! function_exists('photo_thumb')) {
    function photo_thumb($path) {
        return photo(photo_thumb_path($path));
    }
}
if (! function_exists('photo_thumb_path')) {
    function photo_thumb_path($path) {
        return preg_replace("/^(.+)\.(.+)$/", "$1t.$2", $path);
    }
}
if (! function_exists('photo')) {
    function photo($path) {
        return asset('uploads/photos' . DIRECTORY_SEPARATOR . $path).webp_or_jpeg();
    }
}
if (! function_exists('gravatar')) {
    function gravatar($email) {
        return 'https://www.gravatar.com/avatar/' . md5($email) . '?d=retro';
    }
}
if (! function_exists('brl_format')) {
    function brl_format($number) {
        return number_format($number, 2, ",", ".");
    }
}
if (! function_exists('float_from_brl')) {
    function float_from_brl($number) {
        $withoutDot = preg_replace("/\./", "", $number);
        return preg_replace("/,/", ".", $withoutDot);
    }
}

if (! function_exists('brl_without_zero')) {
    function brl_without_zero($number) {
        $format = "R$ " . brl_format($number);
        if (substr($format, -3, 3) == ",00") {
            return substr($format, 0, strlen($format) - 3);
        }
        return $format;
    }
}
