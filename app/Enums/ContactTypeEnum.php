<?php

namespace App\Enums;

class ContactTypeEnum
{
    public const INSTAGRAM = 0;
    public const WHATSAPP = 1;
    public const FACEBOOK = 2;
    public const GOOGLE = 3;
    public static $all = [
        self::INSTAGRAM,
        self::WHATSAPP,
        self::FACEBOOK,
        self::GOOGLE,
    ];
    public static $name = [
        self::INSTAGRAM => 'Instagram',
        self::WHATSAPP => 'WhatsApp',
        self::FACEBOOK => 'Facebook',
        self::GOOGLE => 'Gmail',
    ];
    public static $slug = [
        self::INSTAGRAM => 'instagram',
        self::WHATSAPP => 'whatsapp',
        self::FACEBOOK => 'facebook',
        self::GOOGLE => 'gmail',
    ];
}
