<div class="w-full md:w-1/4 my-8 md:my-0 md:ml-8 p-8 bg-white rounded flex flex-col justify-between md:justify-start sm:flex-row md:flex-col">
  <div class="mb-4 sm:mb-0 md:mb-4">
    <i class="icon-pencil text-gray-600 mr-2"></i><a href="{{ route("users.edit-data") }}">Meus dados</a>
  </div>
  <div class="mb-4 sm:mb-0 md:mb-4">
    <i class="icon-inbox text-gray-600 mr-2"></i><a href="{{ route("users.edit-contacts") }}">Formas de contato</a>
  </div>
  <div class="mb-4 sm:mb-0 md:mb-4">
    <i class="icon-key text-gray-600 mr-2"></i><a href="{{ route("users.edit-password") }}">Alterar senha</a>
  </div>
</div>
