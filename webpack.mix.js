const mix = require('laravel-mix');
const tailwindcss = require('tailwindcss');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js').extract(['alpinejs'])
    .postCss('resources/css/main.css', 'public/css', [
        tailwindcss('./tailwind.config.js')
    ]).options({
        processCssUrls: false
    })
mix.postCss('resources/css/admin.css', 'public/css/admin.css', [
    tailwindcss('./tailwind-admin.config.js')
]).options({
    processCssUrls: false
})
