<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class EditUserPassword extends FormRequest
{
    public function authorize()
    {
        return $this->user()->can("update", $this->user());
    }

    public function rules()
    {
        return [
            "old_password" => "required|password|different:new_password",
            "new_password" => "required|min:6",
            "confirm_new_password" => "required|same:new_password",
        ];
    }
}
