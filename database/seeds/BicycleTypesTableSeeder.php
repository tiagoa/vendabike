<?php

use App\BicycleType;
use Illuminate\Database\Seeder;

class BicycleTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bicycle_types')->delete();
        BicycleType::create(['name' => 'Speed', 'slug' => 'speed']);
        BicycleType::create(['name' => 'MTB', 'slug' => 'mtb']);
        BicycleType::create(['name' => 'Fixa / Single', 'slug' => 'fixa-single']);
        BicycleType::create(['name' => 'TT / Triatlon', 'slug' => 'tt-triatlon']);
    }
}
