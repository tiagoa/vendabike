@extends('layouts.app')

@section('content')
<div class="flex flex-row justify-center px-4 lg:px-0">
  <div class="w-full lg:w-2/3">
    {!! Form::open(["route"=>"password.email", "class"=>"bg-white rounded-lg p-8 w-full md:w-2/3 mx-auto"]) !!}
      <h2 class="font-bold text-2xl mb-6 text-blue-800">Alterar senha</h2>
      <p>Preencha seu e-mail e receba um link para alterar sua senha</p>
      @csrf
      <div class="mb-4 mt-6 flex flex-col">
        <label>E-mail</label>
        <input type="text" name="email" class="border-2 rounded-lg py-2 px-4">
        @error('email')
          <div class="text-red-500">{{ $message }}</div>
        @enderror
      </div>
      <div clas="align-right">
        <button class="cursor-pointer bg-teal-600 hover:bg-teal-700 transition duration-300 w-full sm:w-auto hover:bg-teal-700 text-white font-bold py-2 px-4 rounded-lg">Enviar link</button>
      </div>
    {!! Form::close() !!}
  </div>
</div>
@endsection
