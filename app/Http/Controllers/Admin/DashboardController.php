<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        return view("admin.dashboard");
    }

    public function generateSitemap()
    {
        Artisan::call("sitemap:generate");
        return redirect()->route("admin.dashboard")
            ->with("status", "Sitemap gerado. <a href=\"/sitemap.xml\" target=\"_blank\">Veja aqui</a>");
    }
}
