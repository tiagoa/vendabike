<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'Home')->name("home");
Route::get('/home', 'Home');
Route::get('/feed', 'AdsController@feed');
Route::view('/entrar', 'users.get-in')->name('getIn');
Route::view('/login', 'users.get-in')->name('login');
Route::post('/cadastrar', 'UsersController@signUp')->name('signUp');
Route::post('/entrar', 'UsersController@signIn')->name('signIn');
Route::get("/perfil/{user}", "UsersController@profile")->where("user", "[0-9]+")->name("users.profile");
Route::get('/anuncio/{ad}/{slug}', 'AdsController@show')->where("ad", "[0-9]+")->name("ads.show");
Route::get('/comprar-bicicleta-{bikeType:slug}', 'AdsController@bicycleType')->name("ads.bike-type");
Route::get('/bike-{slug}', 'AdsController@typeAndBrand')->name("ads.type-brand");
Route::get('/bicicletas-marca-{brand:slug}', 'AdsController@brand')->name("ads.brand");
Route::get('/entre-em-contato', 'ContactFormController@index')->name("contactForm");
Route::post('/entre-em-contato', 'ContactFormController@submit');
Route::get('/mensagem-enviada', 'ContactFormController@success');

Route::get('login/instagram', 'SocialLoginController@redirectToInstagramProvider')->name("login.instagram");
Route::get('login/instagram/callback', 'SocialLoginController@handleInstagramProviderCallback');
Route::get('login/facebook', 'SocialLoginController@redirectToFacebookProvider')->name("login.facebook");
Route::get('login/facebook/callback', 'SocialLoginController@handleFacebookProviderCallback');
Route::get('login/google', 'SocialLoginController@redirectToGoogleProvider')->name("login.google");
Route::get('login/google/callback', 'SocialLoginController@handleGoogleProviderCallback');

Route::namespace("Auth")->group(function () {
    Route::post("/recuperar-senha", "ForgotPasswordController@sendResetLinkEmail")->name("password.email");
    Route::post("/alterar-senha", "ResetPasswordController@reset")->name("password.update");
    Route::get("/recuperar-senha", "ForgotPasswordController@showLinkRequestForm")->name("password.request");
    Route::get("/redefinir-senha/{token}", "ResetPasswordController@showResetForm")->name("password.reset");
});

Route::get('/marcas/buscar/{brand}', 'BrandsController@search')->name("brands.search");
Route::get('/cidade/cep/{zipCode}', 'CitiesController@getCityByZipCode')->name("cities.zipcode");

Route::middleware("auth")->group(function () {
    Route::resource('ads', 'AdsController')->except("show");
    Route::get('/perfil/editar', 'UsersController@editData')->name("users.edit-data");
    Route::put('/perfil/editar', 'UsersController@updateData')->name("users.update-data");
    Route::get('/perfil/editar/contatos', 'UsersController@editContacts')->name("users.edit-contacts");
    Route::put('/perfil/editar/contatos', 'UsersController@updateContacts')->name("users.update-contacts");
    Route::get('/perfil/editar/senha', 'UsersController@editPassword')->name("users.edit-password");
    Route::put('/perfil/editar/senha', 'UsersController@updatePassword')->name("users.update-password");
    Route::get('/sair', 'UsersController@logOut')->name('logout');
});

Route::namespace("Admin")->prefix('admin')->group(function () {
    Route::view("/", "admin.login")->name("admin.index");
    Route::post("/login", "LoginController@login")->name("admin.login");
});

Route::namespace("Admin")->prefix('admin')->name("admin.")->middleware("auth:admin")->group(function () {
    Route::get('/dashboard', 'DashboardController@index')->name("dashboard");
    Route::get('/sitemap', 'DashboardController@generateSitemap')->name("sitemap");
    Route::get('/sair', 'LoginController@logOut')->name('logout');
    Route::resource('ads', 'AdsController');
    Route::post('/ads/import', 'AdsController@import');
    Route::post('/ads/delete-all', 'AdsController@destroyAll');
    Route::resource('brands', 'BrandsController');
    Route::resource('users', 'UsersController');
});
