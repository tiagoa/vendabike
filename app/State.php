<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $primaryKey = 'uf';
    public $incrementing = false;
    protected $keyType = 'string';
    public $timestamps = false;
}
