@extends('layouts.admin')
@section('content')
<div class="bg-white rounded-lg p-8">
  <a href="{{ route("admin.brands.create") }}" class="inline-block bg-blue-400 hover:bg-blue-500 text-white font-bold py-2 px-4 rounded-lg text-center">Cadastrar marca</a>
  <h2 class="mt-6 text-2xl font-bold text-gray-500 mb-6">{{ $brands->count() }} Marcas</h2>
  <table class="w-full bg-white">
    <thead>
      <tr class="text-left">
        <th class="text-center">#</th>
        <th>Marca</th>
        <th>Anúncios</th>
        <th>Cadastrado em</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach ($brands as $brand)
      <tr class="bg-{{ $loop->index % 2 == 0 ? "white" : "gray-200" }}">
        <td class="text-center">{{ $brand->id }}</td>
        <td>
          <span
          class="inline-block w-4 h-4 ml-2 bg-no-repeat bg-center bg-contain"
          style="background-image:url('{{ asset("img/brands/".$brand->slug.".png") }}')"
          ></span>
          {{ $brand->name }}
        </td>
        <td>{{ $brand->ads_count }}</td>
        <td>{{ $brand->created_at }}</td>
        <td>
          <div class="opacity-0 hover:opacity-100 flex flex row">
            <a href="{{ route("admin.brands.edit", ["brand"=>$brand]) }}">editar</a>
            {!! Form::model($brand, ["route"=>["admin.brands.destroy", "brand"=>$brand], "method"=>"delete"]) !!}
            <input type="submit" value="deletar" class="bg-transparent ml-2">
            {!! Form::close() !!}
          </div>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
@endsection
