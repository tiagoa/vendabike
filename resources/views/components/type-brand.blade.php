@foreach ($bikeByTypeAndBrand as $type => $brands)
<h2 class="text-xl font-bold text-blue-800 my-6 text-center sm:text-left">Marcas de bicicletas {{$type}}:</h2>
<div class="flex flex-row justify-center sm:justify-start flex-wrap">
  @foreach ($brands as $brand)
    <a href="{{ route('ads.type-brand', ['slug'=>$brand->type_slug.'-'.$brand->brand_slug]) }}"
      class="mx-4 sm:mr-4 mb-6 flex flex-row w-full sm:w-auto items-center border-white border bg-white bg-white rounded-lg hover:border-blue-500 hover:shadow-lg transition duration-300"
      title="Ver bicicletas {{$type}} da marca {{$brand->brand}}"
    >
      <span
        class="w-8 h-8 ml-4 bg-no-repeat bg-center"
        style="background-image:url('/img/brands/{{$brand->brand_slug}}.png')"
      ></span>
      <span class="p-4">
      Bike {{ $brand->type . ' ' . $brand->brand}}
      </span>
    </a>
@endforeach
</div>
@endforeach
