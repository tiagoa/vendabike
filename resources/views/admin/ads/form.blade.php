@extends('layouts.admin')
@section('content')
@if ($ad)
{!! Form::model($ad, ['route' => ['admin.ads.update', $ad->id], "files" => true, "class"=>"px-4 sm:px-0", 'method' => 'put', 'x-data'=>"{'is_bike':".($ad->bike ? 1 : 0)."}"]) !!}
  <h2 class="font-bold text-2xl text-blue-800 mb-4">Atualizar anúncio</h2>
  {!! Form::hidden('id', $ad->id) !!}
@else
{!! Form::open(['route' => 'admin.ads.store', 'files' => true, "class"=>"px-4 sm:px-0", 'x-data'=>"{'is_bike':".(old("category_id") ? old("category_id") : 1) ."}"]) !!}
  <h2 class="font-bold text-2xl text-blue-800 mb-4">Publicar novo anúncio</h2>
@endif
  <div x-data="imageData()" class="mb-4">
    @if($ad && $ad->photos->count())
    <div :class="{'hidden': toRemove.length == 6}">
      <h3 class="font-bold text-blue-700">Fotos atuais <span class="text-gray-500 font-normal">(Clique para remover)</span></h3>
      <div class="flex flex-row flex-wrap mt-2">
        @foreach($ad->photos as $photo)
        <div
          x-ref="photo{{$photo->id}}"
          class="photo cursor-pointer bg-cover bg-center bg-no-repeat max-w-xs w-1/3 sm:w-1/6 h-24 sm:h-32 flex items-center justify-center duration-300"
          style="background-image:url('{{photo_thumb($photo->file)}}')"
          title="Remover esta foto"
          @click="remove({{$photo->id}})"
        >
          <i class="icon-trash text-white text-5xl duration-300"></i>
        </div>
        @endforeach
      </div>
    </div>
    <template x-for="(remove, i) in toRemove" :key="i">
      <input type="hidden" name="deletePhotos[]" x-model="toRemove[i].id">
    </template>
    @endif
    <h3 class="font-bold text-blue-700 mt-4" x-show="isEdit && available > 0">Novas fotos</h3>
    <div class="flex flex-row flex-wrap mt-2">
      <template x-for="(file, i) in previewUrl" :key="i">
        <div
          class="bg-cover bg-center bg-no-repeat max-w-xs w-1/3 sm:w-1/6 h-24 sm:h-32"
          :style="'background-image:url('+file+')'"
        ></div>
      </template>
      <label
        x-show="(isEdit && available > 0 && previewUrl.length < available) || (!isEdit && previewUrl.length < available)"
        for="photos"
        class="flex flex-col font-bold text-lg text-gray-600 flex-1 justify-center text-center p-4 bg-white border-dashed border-2 cursor-pointer"
        x-text="'Você pode escolher até ' +(isEdit ? (available > 1 ? available + ' fotos' : ' 1 foto') : '6 fotos')"
      ></label>
      {!! Form::file('photos[]', ['multiple' => true, "id"=>"photos", "@change"=>"updatePreview()", "class"=>"hidden"]) !!}
    </div>
    <div x-show="previewUrl.length > 0" class="inline-block mt-1 text-center w-full sm:w-auto bg-red-400 hover:bg-red-500 text-white font-bold py-2 px-4 rounded-lg" @click="clear">Remover fotos</div>
  </div>
@if ($ad)
  @if ($ad->bike)
  <div class="mb-4 flex flex-col md:flex-row">
    {!! Form::hidden('bike_id', $ad->bike->id) !!}
    @foreach ($bikeTypes as $bikeType)
      <input type="radio" id="type{{$loop->index}}" name="bicycle_type_id" value="{{$bikeType->id}}" class="hidden" {{$ad->bike->bicycle_type_id == $bikeType->id ? 'checked' : ''}}>
      <label for="type{{$loop->index}}" class="cursor-pointer border-gray-700 hover:bg-blue-300 text-gray-700 border-2 rounded-full whitespace-no-wrap radio flex mr-4 flex-row px-4 py-2 items-center duration-300">
      <i class="icon-ok-circled mr-2"></i>
      <i class="icon-circle mr-2"></i>
      {{$bikeType->name}}
    </label>
    @endforeach
  </div>
  @endif
@else
  <div class="mb-4 flex flex-row">
    @foreach ($categories as $category)
    <input type="radio" x-model="is_bike" id="category{{$loop->index}}" name="category_id" value="{{$category->id}}" class="hidden" {{(old('category_id') == $category->id) ? 'checked' : ($loop->index == 0  ? 'checked' : '')}}>
    <label for="category{{$loop->index}}" class="cursor-pointer border-gray-700 hover:bg-blue-300 text-gray-700 border-2 rounded-full whitespace-no-wrap radio flex mr-4 flex-row px-4 py-2 items-center duration-300">
      <i class="icon-{{$category->slug}} mr-2"></i>
      {{$category->name}}
    </label>
    @endforeach
  </div>
  <div x-show="is_bike == 1" class="flex flex-row overflow-x-auto">
    @foreach ($bikeTypes as $bikeType)
    <input type="radio" id="type{{$loop->index}}" name="bicycle_type_id" value="{{$bikeType->id}}" class="hidden" {{(old('bicycle_type_id') == $bikeType->id) ? 'checked' : ($loop->index == 0  ? 'checked' : '')}}>
    <label for="type{{$loop->index}}" class="cursor-pointer border-gray-700 hover:bg-blue-300 text-gray-700 border-2 rounded-full whitespace-no-wrap radio flex mr-4 flex-row mb-4 px-4 py-2 items-center duration-300">
      <i class="icon-ok-circled mr-2"></i>
      <i class="icon-circle mr-2"></i>
      {{$bikeType->name}}
    </label>
    @endforeach
  </div>
  @endif
  <div class="flex flex-col sm:flex-row">
    <div class="mb-4 flex flex-col w-1/3 mr-4">
      {!! Form::label('user_id', 'Usuário', ["class"=>"font-bold"]) !!}
      {!! Form::select('user_id', $users, null, ['class' => 'border-2 rounded py-2 px-4', "placeholder"=>"Selecione"]) !!}
    </div>
    <div class="mb-4 flex flex-col w-1/3 ml-4">
      {!! Form::label('instagram', 'Usuário IG', ["class"=>"font-bold"]) !!}
      {!! Form::text('instagram', $ad && isset($ad->user->contacts[0]) ? $ad->user->contacts[0]->value : null, ['class' => 'border-2 rounded py-2 px-4']) !!}
    </div>
    <div class="mb-4 flex flex-col w-1/3 ml-4">
      {!! Form::label('whatsapp', 'WhatsApp', ["class"=>"font-bold"]) !!}
      {!! Form::text('whatsapp', $ad && isset($ad->user->contacts[1]) ? $ad->user->contacts[1]->value : null, ['class' => 'border-2 rounded py-2 px-4']) !!}
    </div>
  </div>
  <div class="flex flex-col sm:flex-row">
    <div class="mb-4 flex flex-col w-full">
      {!! Form::label('source', 'Fonte', ["class"=>"font-bold"]) !!}
      {!! Form::text('source', null, ['class' => 'border-2 rounded py-2 px-4']) !!}
      @error('source')
        <div class="text-red-500">{{ $message }}</div>
      @enderror
    </div>
  </div>
  <div class="flex flex-col sm:flex-row">
    <div class="mb-4 flex flex-col sm:mr-4 sm:w-2/3">
      {!! Form::label('description', 'Descrição *', ["class"=>"font-bold"]) !!}
      {!! Form::text('description', null, ['class' => 'border-2 rounded py-2 px-4', "required" => true]) !!}
      @error('description')
        <div class="text-red-500">{{ $message }}</div>
      @enderror
    </div>
    <div class="mb-4 flex flex-col sm:ml-4 sm:w-1/3" x-data="initPrice()">
      {!! Form::label('price', 'Preço *', ["class"=>"font-bold"]) !!}
      {!! Form::text('price', $ad ? brl_format($ad->price) : null, ["class" => "border-2 rounded py-2 px-4", "required" => true, "@keypress"=>"format(\$event)", "x-model" => "price"]) !!}
      @error("price")
        <div class="text-red-500">{{ $message }}</div>
      @enderror
    </div>
  </div>
  <div class="flex flex-col sm:flex-row">
    <div class="mb-4 flex flex-col sm:mr-4 sm:w-1/2" x-data="initBrand()">
      {!! Form::label('brand', 'Marca *', ["class"=>"font-bold"]) !!}
      <input type="hidden" name="brand_id" x-model="brand_id">
      {!! Form::text('brand', null, ['class' => 'border-2 rounded py-2 px-4', "@focus"=>"focus = true", "@blur"=>"focus = false", "@input.debounce.250"=>"search()", "x-model"=>"brand", "required" => true, "autocomplete"=>"off"]) !!}
      @error('brand')
        <div class="text-red-500">{{ $message }}</div>
      @enderror
      <div class="relative">
        <div class="absolute rounded-lg shadow-lg bg-white top-0 py-2 flex flex-col" x-show="focus && brands.length > 0" style="dispĺay:none">
          <template x-for="(brandI, i) in brands" :key="i">
            <div
              class="cursor-pointer hover:bg-blue-300 px-4 py-3"
              x-text="brandI.name"
              @mouseover="brand_id = brandI.id; brand = brandI.name"
            ></div>
          </template>
        </div>
      </div>
    </div>
    <div class="mb-4 flex flex-col sm:ml-4 sm:w-1/2">
      {!! Form::label('usage_time', 'Tempo de uso') !!}
      {!! Form::text('usage_time', null, ['class' => 'border-2 rounded py-2 px-4']) !!}
      @error('usage_time')
        <div class="text-red-500">{{ $message }}</div>
      @enderror
    </div>
  </div>
  <div x-show="is_bike == 1" class="flex flex-col md:flex-row md:flex-wrap md:-mx-4">
    <div class="mb-4 flex flex-col md:px-4 md:w-1/3">
      {!! Form::label('frame_size', 'Quadro / Tamanho') !!}
      {!! Form::text('frame_size', $ad && $ad->bike ? $ad->bike->frame_size : null, ['class' => 'border-2 rounded-lg py-2 px-4']) !!}
    </div>
    <div class="mb-4 flex flex-col md:px-4 md:w-1/3">
      {!! Form::label('fork_headset', 'Garfo / Headset') !!}
      {!! Form::text('fork_headset', $ad && $ad->bike ? $ad->bike->fork_headset : null, ['class' => 'border-2 rounded-lg py-2 px-4']) !!}
    </div>
    <div class="mb-4 flex flex-col md:px-4 md:w-1/3">
      {!! Form::label('handlebars_stem', 'Guidão / Avanço') !!}
      {!! Form::text('handlebars_stem', $ad && $ad->bike ? $ad->bike->handlebars_stem : null, ['class' => 'border-2 rounded-lg py-2 px-4']) !!}
    </div>
    <div class="mb-4 flex flex-col md:px-4 md:w-1/3">
      {!! Form::label('groupset', 'Câmbio dianteiro / Câmbio traseiro') !!}
      {!! Form::text('groupset', $ad && $ad->bike ? $ad->bike->groupset : null, ['class' => 'border-2 rounded-lg py-2 px-4']) !!}
    </div>
    <div class="mb-4 flex flex-col md:px-4 md:w-1/3">
      {!! Form::label('brakes', 'Freio dianteiro / Freio traseiro') !!}
      {!! Form::text('brakes', $ad && $ad->bike ? $ad->bike->brakes : null, ['class' => 'border-2 rounded-lg py-2 px-4']) !!}
    </div>
    <div class="mb-4 flex flex-col md:px-4 md:w-1/3">
      {!! Form::label('pedals', 'Pedais') !!}
      {!! Form::text('pedals', $ad && $ad->bike ? $ad->bike->pedals : null, ['class' => 'border-2 rounded-lg py-2 px-4']) !!}
    </div>
    <div class="mb-4 flex flex-col md:px-4 md:w-1/3">
      {!! Form::label('saddle_seatpost', 'Selim / Canote') !!}
      {!! Form::text('saddle_seatpost', $ad && $ad->bike ? $ad->bike->saddle_seatpost : null, ['class' => 'border-2 rounded-lg py-2 px-4']) !!}
    </div>
    <div class="mb-4 flex flex-col md:px-4 md:w-1/3">
      {!! Form::label('front_wheel_hub_tire', 'Roda / Cubo / Pneu dianteiro') !!}
      {!! Form::text('front_wheel_hub_tire', $ad && $ad->bike ? $ad->bike->front_wheel_hub_tire : null, ['class' => 'border-2 rounded-lg py-2 px-4']) !!}
    </div>
    <div class="mb-4 flex flex-col md:px-4 md:w-1/3">
      {!! Form::label('rear_wheel_hub_tire', 'Roda / Cubo / Pneu traseiro') !!}
      {!! Form::text('rear_wheel_hub_tire', $ad && $ad->bike ? $ad->bike->rear_wheel_hub_tire : null, ['class' => 'border-2 rounded-lg py-2 px-4']) !!}
    </div>
  </div>
  <div class="mb-4 flex flex-col md:flex-row">
    <input type="checkbox" name="receipt" value="1" id="receipt" class="hidden" {{ $ad && $ad->receipt == 1 ? 'checked' : (old('receipt') ? 'checked' : '') }}>
    <label for="receipt" class="cursor-pointer border-gray-700 hover:bg-blue-300 text-gray-700 border-2 rounded-full whitespace-no-wrap radio flex mr-4 flex-row mb-4 px-4 py-2 items-center duration-300">
      <i class="icon-file-alt mr-2"></i>
      Possuo nota fiscal
    </label>
    <input type="checkbox" name="negotiation" value="1" id="negotiation" class="hidden" {{($ad && $ad->negotiation == 1) ? 'checked' : (old('negotiation') ? 'checked' : '')}}>
    <label for="negotiation" class="cursor-pointer border-gray-700 hover:bg-blue-300 text-gray-700 border-2 rounded-full whitespace-no-wrap radio flex mr-4 flex-row mb-4 px-4 py-2 items-center duration-300">
      <i class="icon-handshake mr-2"></i>
      Aceito negociar o valor ou troca
    </label>
  </div>
  <div class="mb-4 flex flex-col">
    {!! Form::label('details', 'Detalhes *', ["class"=>"font-bold"]) !!}
    {!! Form::textarea('details', null, ['class'=>'border-2 rounded py-2 px-4', "request" => true]) !!}
    @error('details')
      <div class="text-red-500">{{ $message }}</div>
    @enderror
  </div>
  <div class="flex flex-row">
@if ($ad)
    <button class="bg-teal-600 hover:bg-teal-700 text-white font-bold py-2 px-4 rounded-lg">Atualizar anúncio</button>
    <a
      href="{{ route("admin.ads.show", ["ad"=>$ad, "slug"=>$ad->slug])}}"
      class="hover:underline text-gray-800 font-bold py-2 px-4 text-center"
    >Cancelar</a>
@else
  <button class="bg-teal-600 w-full sm:w-auto hover:bg-teal-700 text-white font-bold py-2 px-4 rounded-lg">Publicar anúncio</button>
@endif
  {!! Form::select('status', $status, null, ['class' => 'border-2 rounded py-2 px-4', "placeholder"=>"Selecione"]) !!}
  </div>
  {!! Form::close() !!}
  <style type="text/css">
    .photo i {
      opacity: 0;
    }
    .photo:hover > i {
      opacity: 1;
    }
    label.radio .icon-ok-circled {
      display: none;
    }
    input[type="radio"]:checked + label.radio,input[type="checkbox"]:checked + label.radio {
      background-color: #63b3ed;
      border-color:#63b3ed;
      color: #ffffff;
    }
    input[type="radio"]:checked + label.radio .icon-circle {
      display:none
    }
    input[type="radio"]:checked + label.radio .icon-ok-circled {
      display:inline-block
    }
  </style>
  <script type="text/javascript">
  function initPrice() {
    return {
      price: "{{ $ad ? brl_format($ad->price) : (old('price') ? old('price') : "0,00") }}",
      format: function(e) {
        let len = this.price.length;
        let key = '';
        let i = 0;
        const strCheck = '0123456789';
        let aux = '';
        const whichCode = (window.addEventListener) ? e.which : e.keyCode;
        // 13=enter, 8=backspace as demais retornam 0(zero)
        // whichCode==0 faz com que seja possivel usar todas as teclas como delete, setas, etc
        if ((whichCode === 13) || (whichCode === 0) || (whichCode === 8)) {
          return true;
        }
        key = String.fromCharCode(whichCode); // Valor para o código da Chave

        if (strCheck.indexOf(key) === -1) {
          e.preventDefault();
          return;
        } // Chave inválida

        for (i = 0; i < len; i += 1) {
          if ((this.price.charAt(i) !== '0') && (this.price.charAt(i) !== ",")) {
            break;
          }
        }
        for (; i < len; i += 1) {
          if (strCheck.indexOf(this.price.charAt(i)) !== -1) {
            aux += this.price.charAt(i);
          }
        }
        aux += key;
        len = aux.length;
        if (len === 0) {
          this.price = '';
        }
        if (len === 1) {
          this.price = '0,0' + aux;
        }
        if (len === 2) {
          this.price = '0,' + aux;
        }
        if (len > 2) {
          let aux2 = '';
          let len2 = 0;
          let j = 0;
          for (j = 0, i = len - 3; i >= 0; i -= 1) {
            if (j === 3) {
              aux2 += ".";
              j = 0;
            }
            aux2 += aux.charAt(i);
            j += 1;
          }
          this.price = '';
          len2 = aux2.length;
          for (i = len2 - 1; i >= 0; i -= 1) {
            this.price += aux2.charAt(i);
          }
          this.price += "," + aux.substr(len - 2, len);
        }
        e.preventDefault();
      }
    }
  }

  function initBrand() {
    return {
      brand: '{{ $ad ? $ad->brand && $ad->brand->name : ""}}',
      focus: false,
      brands: [],
      loading: false,
      brand_id: {{ $ad && $ad->brand_id ? $ad->brand_id : "null" }},
      error: false,
      search: function() {
        this.brands = [];
        this.brand_id = null;
        this.error = false;
        if (!this.brand || this.brand.length < 3) {
          return;
        }
        this.loading = true;

        const url = document.getElementsByTagName('base')[0].getAttribute('href')+'/marcas/buscar/'+this.brand;

        fetch(url)
          .then((response) => response.json())
          .then((data) => {
            this.brands = data;
            this.loading = false;
          }).catch(e => {
            console.log('eeeerr',e);
            this.error = true;
            this.loading = false;
          });
      }
    };
  }

  function imageData() {
    return {
      available: {{ $ad ? 6 - $ad->photos->count() : 6}},
      uploaded: {{ $ad ? $ad->photos->count() : 0}},
      isEdit: {{ $ad ? 'true' : 'false'}},
      previewUrl: [],
      toRemove: [],
      updatePreview() {
        this.previewUrl = [];
        let files = document.getElementById("photos").files;
        const qtd = files.length;
        if (qtd > this.available) {
          this.clear();
          return;
        }
        for (var i = 0; i < qtd; i++) {
          const reader = new FileReader();
          reader.onload = e => {
            this.previewUrl.push(e.target.result);
          };
          reader.readAsDataURL(files[i]);
        }
      },
      clear() {
        document.getElementById("photos").value = null;
        this.previewUrl = [];
        this.available = 6-this.uploaded;
      },
      remove(id) {
        this.toRemove.push({id:id});
        this.$refs['photo'+id].style.width = "0px";
        this.uploaded--;
        this.available++;
      }
    };
  }
  </script>
@endsection
