<?php

namespace App;

use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;
    use FormAccessible;
    use CanResetPassword;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', "facebook_id", "google_id", "instagram_id", "avatar"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getNameAttribute() {
        if ($this->attributes['name']) {
            return $this->attributes['name'];
        }
        return $this->attributes['email'];
    }

    public function formNameAttribute($value)
    {
        return $value;
    }

    public function ads() {
        return $this->hasMany("App\Ad");
    }

    public function city() {
        return $this->belongsTo("App\City");
    }

    public function contacts() {
        return $this->hasMany("App\UserContact");
    }


}
