<?php

namespace App\Http\Requests\Ad;

use App\Ad;
use Illuminate\Foundation\Http\FormRequest;

class EditAd extends FormRequest
{
    public function authorize()
    {
        // dd($this->route("ad"));
        $ad = Ad::find($this->route("ad")->id);
        return $this->user()->can("update", $ad);
    }

    public function rules()
    {
        return [
            'description' => 'required|min:10',
            'id' => 'required|exists:ads,id',
            'photos' => 'array|max_photos:6',
            'photos.*' => 'mimes:jpeg,png|max:1024',
            'details' => 'required',
            'price' => 'required|regex:/^(\d{1,3}\.)?(\d{1,3}\.)?\d{1,3},\d{2}$/',
            'bicycle_type_id' => 'integer|exists:bicycle_types,id',
            'bike_id' => 'integer|exists:bicycles,id',
            'brand' => 'required',
            'brand_id' => 'nullable|integer|exists:brands,id',
        ];
    }
}
