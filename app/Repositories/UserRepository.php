<?php

namespace App\Repositories;

use App\Enums\ContactTypeEnum;
use App\User;
use App\UserContact;
use Illuminate\Support\Facades\Hash;

class UserRepository {

    public function store($credentials)
    {
        $credentials["password"] = Hash::make($credentials["password"]);
        return User::create($credentials);
    }

    public function findOrCreateUserFromInstagram($socialUser)
    {
        $authUser = User::where("instagram_id", $socialUser->id)->first();
        if ($authUser) {
            return $authUser;
        }

        $authUser = User::whereHas("contacts", function($query) use ($socialUser) {
            $query->where("type", ContactTypeEnum::INSTAGRAM)
                ->where("value", $socialUser->nickname);
        })->first();

        if ($authUser) {
            $authUser->instagram_id = $socialUser->id;
            $authUser->save();
            return $authUser;
        }

        $newUser = User::create([
            "name" => $socialUser->nickname,
            "instagram_id" => $socialUser->id,
            "avatar" => getProfilePictureFromInstagramByUsername($socialUser->nickname)
        ]);
        $newUser->contacts()->create([
            "type" => ContactTypeEnum::INSTAGRAM,
            "value" => $socialUser->nickname
        ]);
        return $newUser;
    }

    public function findOrCreateUserFromFacebook($socialUser)
    {
        $authUser = User::where("facebook_id", $socialUser->id)->first();
        if ($authUser) {
            return $authUser;
        }

        $authUser = User::whereHas("contacts", function ($query) use ($socialUser) {
            $query->where("type", ContactTypeEnum::FACEBOOK)
                ->where("value", $socialUser->profileUrl);
        })->first();

        if ($authUser) {
            $authUser->facebook_id = $socialUser->id;
            if ($authUser->email == null && $socialUser->email) {
                $authUser->email = $socialUser->email;
            }
            $authUser->save();
            return $authUser;
        }

        $newContact = [
            "type" => ContactTypeEnum::FACEBOOK,
            "value" => $socialUser->profileUrl
        ];

        if ($socialUser->email) {
            $authUser = User::where("email", $socialUser->email)->first();

            if ($authUser) {
                $authUser->facebook_id = $socialUser->id;
                $authUser->contacts()->create($newContact);
                $authUser->save();
                return $authUser;
            }
        }

        $newUser = User::create([
            "name" => $socialUser->name,
            "email" => $socialUser->email,
            "facebook_id" => $socialUser->id,
            "avatar" => $socialUser->avatar
        ]);
        $newUser->contacts()->create($newContact);
        return $newUser;
    }

    public function findOrCreateUserFromGoogle($socialUser)
    {
        $authUser = User::where("google_id", $socialUser->id)->first();
        if ($authUser) {
            return $authUser;
        }

        $authUser = User::whereHas("contacts", function($query) use ($socialUser) {
            $query->where("type", ContactTypeEnum::GOOGLE)
                ->where("value", $socialUser->id);
        })->first();

        if ($authUser) {
            $authUser->google_id = $socialUser->id;
            if ($authUser->email == null && $socialUser->email) {
                $authUser->email = $socialUser->email;
            }
            $authUser->save();
            return $authUser;
        }

        $newContact = [
            "type" => ContactTypeEnum::GOOGLE,
            "value" => $socialUser->email
        ];

        if ($socialUser->email) {
            $authUser = User::where("email", $socialUser->email)->first();

            if ($authUser) {
                $authUser->google_id = $socialUser->id;
                $authUser->contacts()->create($newContact);
                $authUser->save();
                return $authUser;
            }
        }

        $newUser = User::create([
            "name" => $socialUser->name,
            "email" => $socialUser->email,
            "google_id" => $socialUser->id,
            "avatar" => $socialUser->avatar
        ]);
        $newUser->contacts()->create($newContact);
        return $newUser;
    }

    public function updateData($request, $user)
    {
        $user->name = $request->name;
        if ($request->email) {
            $user->email = $request->email;
        }
        $user->zipcode = $request->zipcode;
        $user->city_id = $request->city_id;
        $user->save();
    }

    public function updatePassword($request, $user)
    {
        $user->password = Hash::make($request->new_password);
        $user->save();
    }

    public function getUserForEditContacts($user)
    {
        $user->load("contacts");
        $contactTypes = ContactTypeEnum::$all;
        $alreadyHave = [];
        foreach ($user->contacts as $userContact) {
            $alreadyHave[] = $userContact->type;
            $user->contacts[$userContact->type] = $userContact;
        }
        foreach ($contactTypes as $i => $contactType) {
            if (!in_array($contactType, $alreadyHave)) {
                $newUserContact = new UserContact();
                $newUserContact->type = $contactType;
                $user->contacts[$i] = $newUserContact;
                continue;
            }
        }

        return $user;
    }

    public function updateContacts($request, $user)
    {
        foreach ($request->contacts as $contact) {
            if ($contact["id"]) {
                $userContact = UserContact::find($contact["id"]);
                if ($contact["value"] == null) {
                    $userContact->delete();
                    continue;
                }
                $userContact->value = $contact["value"];
                $userContact->save();
                continue;
            }
            if ($contact["value"]) {
                $userContact = new UserContact();
                $userContact->user_id = $user->id;
                $userContact->type = $contact["type"];
                $userContact->value = $contact["value"];
                $userContact->save();
            }
        }
    }

}
