@extends('layouts.app')
@section('content')
<h2 class="text-2xl font-bold text-blue-800 mb-6 text-center sm:text-left">Bicicletas {{ $bikeType->name }} à venda</h2>
@if ($ads->count())
<x-ad-grid :ads="$ads" />
@else
<div class="bg-blue-200 text-blue-900 text-center rounded-lg p-8">Ainda não tem anúncio de bicicletas {{ $bikeType->name }}.</div>
@endif
@endsection
