<?php

namespace App\Http\Controllers\Admin;

use App\Enums\ContactTypeEnum;
use App\User;
use App\Http\Controllers\Controller;
use App\Services\InstagramBot;
use App\UserContact;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::withCount("ads")->get();
        return view("admin.users.index", compact("users"));
    }

    public function create()
    {
        $user = null;
        return view("admin.users.form", compact("user"));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $user = new User();
        $user->name = $request->name;
        $user->save();

        return redirect()->route("admin.users.index");
    }

    public function edit(User $user)
    {
        $user->load("contacts", "city");
        return view("admin.users.form", compact("user"));
    }

    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $user->name = $request->name;
        $user->email = $request->email;
        if ($request->zipcode) {
            $user->zipcode = $request->zipcode;
        }
        if ($request->city_id) {
            $user->city_id = $request->city_id;
        }
        foreach ($request->contacts as $contact) {
            if ($contact["id"]) {
                $userContact = UserContact::find($contact["id"]);
                if ($contact["value"] == null) {
                    $userContact->delete();
                    continue;
                }
                $userContact->value = $contact["value"];
                if ($userContact->type == ContactTypeEnum::INSTAGRAM && $request->import_avatar) {
                    $user->avatar = InstagramBot::getProfilePictureFromInstagramByUsername($userContact->value);
                }
                $userContact->save();
                continue;
            }
        }
        $user->save();

        return redirect()->route("admin.users.index");
    }

    public function destroy(Request $request, User $user)
    {
        if ($user->ads()->withTrashed()->count()) {
            throw ValidationException::withMessages([
                "delete"=>"Usuário não pode ser deletado porque possui anúncio vinculado"
            ]);
        }
        $user->delete();
        return redirect()->route("admin.users.index")->with("status", "Usuário deletado");
    }
}
