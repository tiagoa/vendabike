@extends('layouts.app')
@section('content')
<h2 class="text-2xl font-bold text-blue-800 mb-6 text-center sm:text-left">Bicicletas {{ $brand->name }} à venda</h2>
@if ($ads->count())
<x-ad-grid :ads="$ads" />
@else
<div class="bg-blue-200 text-center rounded-lg p-8">Por enquanto não tem nenhum anúncio de bicicleta da marca {{$brand->name}} disponível para venda.</div>
@endif
<h2 class="text-xl font-bold text-blue-800 my-6 text-center sm:text-left">Outras marcas</h2>
<div class="px-4 flex flex-row justify-center sm:justify-start flex-wrap sm:p-0">
@foreach ($brands as $brand)
  <a
    class="inline-block flex flex-row py-2 pr-4 mr-2 items-center border-white border bg-white sm:mr-6 mb-4 rounded-full hover:shadow-lg hover:border-blue-500 transition duration-300"
    href="{{ route("ads.brand", ["brand" => $brand->slug]) }}"
    title="Ver anúncios de bicicletas da marca {{$brand->name}}"
  >
    <span
      class="w-8 h-8 ml-4 mr-3 bg-no-repeat bg-center"
      style="background-image:url('/img/brands/{{$brand->slug}}.png')"
    ></span>
    {{ $brand->name }}
  </a>
@endforeach
</div>
@endsection
