@extends('layouts.admin')
@section('content')
<div class="bg-white rounded-lg p-8">
  <div class="flex flex-row justify-between mt-6 mb-6">
    <h2 class="text-2xl font-bold text-gray-500">{{ $ads->count() }} Anúncios</h2>
    <a href="{{ route("admin.ads.create") }}" class="inline-block bg-blue-400 hover:bg-blue-500 text-white font-bold py-2 px-4 rounded-lg text-center">Novo Anúncio</a>
    {!! Form::open(["action"=>"Admin\AdsController@import", "class"=>"flex flex-row"]) !!}
    <input type="text" name="url" class="border-2 rounded-lg py-2 px-4">
    <button class="bg-teal-600 hover:bg-teal-700 text-white font-bold py-2 px-4 rounded-lg">Importar</button>
    {!! Form::close() !!}
  </div>
  {!! Form::open(["action"=>"Admin\AdsController@destroyAll"]) !!}
  <table class="w-full">
    <thead>
      <tr class="text-left">
        <th class="text-center">#</th>
        <th>Descrição</th>
        <th>Categoria</th>
        <th>Marca</th>
        <th>Valor</th>
        <th>Situação</th>
        <th>Cadastrado em</th>
        <th>Admin</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach ($ads as $ad)
      <tr class="{{ $loop->index % 2 == 0 ? "bg-gray-100" : "bg-white" }} hover:bg-gray-200">
        <td class="text-center">{{ $ad->id }}</td>
        <td>{{ $ad->description }}</td>
        <td>{{ $ad->category->name }}</td>
        <td>{{ $ad->brand ? $ad->brand->name : '' }}</td>
        <td>{{ $ad->price }}</td>
        <td>{{ $ad->status }}</td>
        <td>{{ $ad->created_at }}</td>
        <td>
        @if(empty($ad->source))
        Não
        @else
        <a href="{{$ad->source}}" target="_blank">Sim</a>
        <input type="checkbox" name="ids[]" value="{{$ad->id}}">
        @endif
        </td>
        <td>
          <div class="opacity-0 hover:opacity-100">
            <a href="{{ route("admin.ads.show", ["ad"=>$ad]) }}">ver</a>
            <a href="{{ route("admin.ads.edit", ["ad"=>$ad]) }}">editar</a>
          </div>
        </td>
      </tr>
      @endforeach
    </tbody>
    <tfoot>
      <tr>
        <td colspan="7"></td>
        <td>
          {!!Form::hidden('sold', $deletionReasonSold)!!}
          <button class="border-2 border-red-500 hover:bg-red-500 text-red-500 hover:text-white font-bold py-2 px-4 rounded-lg">x</button>
        </td>
      </tr>
    </tfoot>
  </table>
  {!! Form::close() !!}
</div>
@endsection
