<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BicycleType extends Model
{
    public $timestamps = false;

    public function bike()
    {
        return $this->hasMany("App\Bicycle");
    }
}
