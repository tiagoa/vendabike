module.exports = {
  purge: {
    mode: 'all',
    content: [
        "resources/views/ads/*.php",
        "resources/views/auth/**/*.php",
        "resources/views/components/*.php",
        "resources/views/contact-form/*.php",
        "resources/views/layouts/app.blade.php",
        "resources/views/layouts/edit-user.blade.php",
        "resources/views/users/*.php",
        "resources/views/*.php",
    ],
    options:{
        whitelist: [
            'border-4',
            'h-0',
            'mb-8'
        ]
    }
  },
  theme: {
    extend: {
      colors: {
        blue: {
          100: '#F0F8FF',
          200: '#BDE1FF',
          300: '#8ACAFF',
          400: '#57B3FF',
          500: '#249CFF',
          600: '#0084F0',
          700: '#0068BD',
          800: '#004C8A',
          900: '#003057'
        },
        teal: {
          100: '#B8FFFB',
          200: '#85FFF9',
          300: '#52FFF6',
          400: '#1FFFF4',
          500: '#00EBDF',
          600: '#00B8AE',
          700: '#00857E',
          800: '#00524E',
          900: '#001F1D'
        },
        green: {
          100: '#D1FFE4',
          200: '#9EFFC6',
          300: '#6BFFA9',
          400: '#38FF8B',
          500: '#05FF6D',
          600: '#00D157',
          700: '#009E42',
          800: '#006B2D',
          900: '#003817'
        },
      }
    },
  },
  variants: {},
  plugins: [],
}
