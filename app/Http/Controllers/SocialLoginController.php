<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use Laravel\Socialite\Facades\Socialite;
use Auth;

class SocialLoginController extends Controller
{
    protected $user;

    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }

    public function redirectToInstagramProvider()
    {
        return Socialite::driver("instagrambasic")->redirect();
    }

    public function handleInstagramProviderCallback()
    {
        $user = Socialite::driver("instagrambasic")->user();
        $authUser = $this->user->findOrCreateUserFromInstagram($user);

        Auth::login($authUser, true);

        return redirect('/');
    }

    public function redirectToFacebookProvider()
    {
        return Socialite::driver("facebook")->scopes(["user_link"])->redirect();
    }

    public function handleFacebookProviderCallback()
    {
        $user = Socialite::driver("facebook")->user();
        $authUser = $this->user->findOrCreateUserFromFacebook($user);

        Auth::login($authUser, true);

        return redirect('/');
    }

    public function redirectToGoogleProvider()
    {
        return Socialite::driver("google")->redirect();
    }

    public function handleGoogleProviderCallback()
    {
        $user = Socialite::driver("google")->user();
        $authUser = $this->user->findOrCreateUserFromGoogle($user);

        Auth::login($authUser, true);

        return redirect('/');
    }
}
