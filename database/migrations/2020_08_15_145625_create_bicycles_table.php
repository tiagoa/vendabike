<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBicyclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bicycles', function (Blueprint $table) {
            $table->id();
            $table->foreignId('ad_id')->constrained();
            $table->foreignId('bicycle_type_id')->nullable()->constrained();
            $table->string('size', 20)->nullable();
            $table->decimal('weight', 2, 2)->nullable();
            $table->integer('year')->nullable();
            $table->string('model', 100)->nullable();
            $table->string('frame_size', 100)->nullable();
            $table->string('fork_headset', 255)->nullable();
            $table->string('handlebars_stem', 255)->nullable();
            $table->string('brakes', 255)->nullable();
            $table->string('groupset', 255)->nullable();
            $table->string('pedals', 255)->nullable();
            $table->string('saddle_seatpost', 255)->nullable();
            $table->string('front_wheel_hub_tire', 255)->nullable();
            $table->string('rear_wheel_hub_tire', 255)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bicycles');
    }
}
