<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <title>Lançador Web</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="{{ route('home') }}">
    <link href="{{ asset("css/admin.css") }}" rel="stylesheet">
    <script src="{{ asset("js/manifest.js") }}" defer></script>
    <script src="{{ asset("js/vendor.js") }}" defer></script>
    <script src="{{ asset("js/app.js") }}" defer></script>
  </head>
  <body class="bg-blue-100">
  {!! Form::open(["route"=>["admin.login"], "class"=>"flex flex-col bg-white p-8 mx-auto w-11/12 sm:w-1/2 lg:w-1/5 mt-8 rounded-lg shadow-xl"]) !!}
  {!! Form::text("username", null, ["placeholder"=>"Login", "autofocus"=>true, "class"=>"border-2 rounded p-2"]) !!}
  {!! Form::password("password", ["placeholder"=>"Senha", "class"=>"border-2 rounded p-2 mt-4"]) !!}
  {!! Form::submit("Entrar", ["class"=>"bg-blue-500 text-white font-bold px-2 py-3 mt-8 rounded"]) !!}
  {!! Form::close() !!}
  </body>
</html>
