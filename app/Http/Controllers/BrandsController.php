<?php

namespace App\Http\Controllers;

use App\Brand;
use Illuminate\Http\Request;

class BrandsController extends Controller
{
    public function search(Request $request, $brand)
    {
        $brands = Brand::select("id", "name")->where('name', 'like', "%".$brand."%")->get();
        return response()->json($brands);
    }
}
