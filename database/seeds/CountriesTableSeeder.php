<?php

use App\Country;
use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('countries')->delete();
        Country::create(['cod' => 'BRA', 'name' => 'Brasil']);
    }
}
