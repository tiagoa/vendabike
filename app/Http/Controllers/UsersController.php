<?php

namespace App\Http\Controllers;

use App\City;
use App\Enums\ContactTypeEnum;
use App\Http\Requests\User\EditUserContacts;
use App\Http\Requests\User\EditUserData;
use App\Http\Requests\User\EditUserPassword;
use App\Http\Requests\User\SignIn;
use App\Http\Requests\User\SignUp;
use App\Repositories\AdRepository;
use App\Repositories\UserRepository;
use App\State;
use App\User;
use Auth;
use Hash;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    protected $user;

    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }
    public function signUp(SignUp $request)
    {
        $credentials = $request->only('email', 'password');

        $this->user->store($credentials);

        if (Auth::attempt($credentials)) {
            return redirect()->intended('/');
        }
        return redirect()->to('/');
    }

    public function signIn(SignIn $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $hour = date("G");
            $greetings = "Boa noite";
            if ($hour >= 6 && $hour < 12) {
                $greetings = "Bom dia";
            }
            if ($hour >= 12 && $hour < 18) {
                $greetings = "Boa tarde";
            }
            $phrases = [
                "Que bom te ver por aqui!",
                "Adoramos sua presença!",
                "Fique à vontade!",
            ];
            return redirect()
                ->intended('/')
                ->with(["status"=>$greetings." ".auth()->user()->name."! ".$phrases[rand(0, count($phrases)-1)]]);
        }
        return redirect()
            ->route("signIn")
            ->withInput()
            ->withErrors(["email"=> "E-mail ou senha está incorreto"]);
    }

    public function profile(User $user)
    {
        $adRepository = new AdRepository();
        $ads = $adRepository->getAdsByUser($user->id);
        return view("users.profile", compact("ads", "user"));
    }

    public function editData()
    {
        $user = auth()->user()->load("city");
        return view("users.edit-data", compact("user"));
    }

    public function updateData(EditUserData $request)
    {
        $this->user->updateData($request, auth()->user());
        return redirect()
            ->route("users.edit-data")
            ->with("status", "Dados atualizados!");
    }

    public function editContacts()
    {
        $user = $this->user->getUserForEditContacts(auth()->user());
        return view("users.edit-contacts", compact("user"));
    }

    public function updateContacts(EditUserContacts $request)
    {
        $this->user->updateContacts($request, auth()->user());
        return redirect()
            ->route("users.edit-contacts")
            ->with("status", "Contatos atualizados!");
    }

    public function editPassword()
    {
        $user = auth()->user();
        return view("users.edit-password", compact("user"));
    }

    public function updatePassword(EditUserPassword $request)
    {
        $this->user->updatePassword($request, auth()->user());
        return redirect()
            ->route("users.edit-password")
            ->with("status", "Senha alterada!");
    }

    public function logout()
    {
        $phrases = [
            "Até mais",
            "Venha mais vezes",
            "Retorne quando quiser",
        ];
        $message = $phrases[rand(0, count($phrases)-1)]. " " .auth()->user()->name."!";
        Auth::logout();
        return redirect()->to('/')->with(["status"=>$message]);
    }
}
