<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->only('username', 'password');

        if (Auth::guard("admin")->attempt($credentials)) {
            return redirect()->route('admin.dashboard');
        }
        return redirect()->route('admin.index');
    }
    public function logout()
    {
        Auth::logout();
        return redirect()->to('/');
    }
}
