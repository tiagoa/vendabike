<?php

namespace App\Http\Requests\User;

use Illuminate\Contracts\Validation\Rule;

class BlacklistEmailRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $ch = curl_init('https://raw.githubusercontent.com/martenson/disposable-email-domains/master/disposable_email_blocklist.conf');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        preg_match('/@(.+)$/', $value, $servidor);
        $temp_mails = explode("\n", $output);

        if ($servidor && sizeof($servidor) > 0) {
            return !in_array($servidor[1], $temp_mails);
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Este domínio de e-mail não pode ser utilizado.';
    }
}
