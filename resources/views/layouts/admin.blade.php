<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <title>Lançador Web</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="{{ route('home') }}">
    <link href="{{ asset("css/admin.css") }}" rel="stylesheet">
    <script src="{{ asset("js/manifest.js") }}" defer></script>
    <script src="{{ asset("js/vendor.js") }}" defer></script>
    <script src="{{ asset("js/app.js") }}" defer></script>
  </head>
  <body class="bg-blue-100">
    <div class="w-full sticky top-0 z-10">
      <div class="w-full bg-blue-900 px-2 sm:px-4">
        <div class="container mx-auto flex flex-row items-center justify-between">
          <a href="/" title="Ir para a página inicial" id="logo" class="text-teal-300 text-2xl font-bold"
          ><img src="/img/logo.png" alt="Vapt!"></a>
          <div class="flex flex-row py-2 sm:py-4 items-start">
            <a href="{{ route('admin.logout') }}" class="text-blue-300 whitespace-no-wrap rounded-lg hover:bg-blue-700 hover:text-blue-100 font-bold py-2 px-2 sm:px-4 mr-2 sm:mr-4">Sair</a>
          </div>
        </div>
      </div>
      <div class="w-full bg-blue-100 mb-8 shadow-md">
        <nav class="container mx-auto flex flex-row items-center overflow-x-auto">
          <a href="{{ route("admin.dashboard") }}" class="text-blue-900 whitespace-no-wrap px-3 sm:px-4 pt-2 sm:pt-4 pb-1 sm:pb-3 border-b-4 hover:text-blue-800 hover:border-blue-800 transition duration-300 border-{{Route::current()->getName() == 'home' ? 'blue-800' : 'transparent' }}"><i class="icon-home"></i></a>
          <a href="{{ route("admin.ads.index") }}" class="text-blue-900 whitespace-no-wrap px-3 sm:px-4 pt-2 sm:pt-4 pb-1 sm:pb-3 hover:text-blue-800 hover:border-blue-800 transition duration-300 border-b-4 border-transparent">Anúncios</a>
          <a href="{{ route("admin.brands.index") }}" class="text-blue-900 whitespace-no-wrap px-3 sm:px-4 pt-2 sm:pt-4 pb-1 sm:pb-3 hover:text-blue-800 hover:border-blue-800 transition duration-300 border-b-4 border-transparent">Marcas</a>
          <a href="{{ route("admin.users.index") }}" class="text-blue-900 whitespace-no-wrap px-3 sm:px-4 pt-2 sm:pt-4 pb-1 sm:pb-3 hover:text-blue-800 hover:border-blue-800 transition duration-300 border-b-4 border-transparent">Usuários</a>
        </nav>
      </div>
    </div>
    <div class="w-full sm:px-4">
      <div class="container mx-auto">
        @if ($errors->any())
        <div class="flex flex-col bg-red-100 mb-8 border-2 text-red-800 border-red-500 px-4 pt-4 rounded-lg">
          @foreach ($errors->all() as $error)
            <div class="mb-4">{{ $error }}</div>
          @endforeach
        </div>
        @endif
        @if (session('status'))
          <div class="flex relative bg-green-100 text-green-800 border-green-600 rounded-lg transition-all mx-4 sm:mx-0 duration-300" :class="{'h-0 overflow-hidden p-0':open==false, 'p-4 pr-10 mb-8 border-2':open==true}" x-data="{open:true}">
          {!! session('status') !!}
          <i class="icon-cancel absolute right-0 top-0 m-4" @click="open=false"></i>
        </div>
        @endif
        @yield('content')
      </div>
    </div>
  </body>
</html>
