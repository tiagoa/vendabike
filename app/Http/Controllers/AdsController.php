<?php

namespace App\Http\Controllers;

use App\Ad;
use App\BicycleType;
use App\Brand;
use App\Http\Requests\Ad\DeleteAd;
use App\Http\Requests\Ad\NewAd;
use App\Http\Requests\Ad\EditAd;
use App\Repositories\AdRepository;
use Request;
use Storage;
use Illuminate\View\View;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AdsController extends Controller
{
    protected $ad;

    public function __construct(AdRepository $ad)
    {
        $this->ad = $ad;
    }

    public function index()
    {
        //
    }

    public function create()
    {
        $ad = null;
        return view("ads.form", compact("ad"));
    }

    public function store(NewAd $request)
    {
        $ad = $this->ad->store($request);
        return redirect()
            ->route("ads.show", ["ad" => $ad->id, "slug"=>$ad->slug])
            ->with("status", "Anúncio publicado!");
    }

    public function bicycleType(BicycleType $bikeType)
    {
        $ads = $this->ad->getAdsByBicycleType($bikeType);
        $title = "Bicicletas " . $bikeType->name . "à venda";
        return view("ads.bike-type", compact("ads", "bikeType", "title"));
    }


    public function brand(Brand $brand)
    {
        $ads = $this->ad->getAdsByBrand($brand);
        $brands = Brand::whereHas("ads.bike")->get();
        $title = "Bicicletas ".$brand->name." à venda";
        return view("ads.brand", compact("ads", "brand", "brands", "title"));
    }

    public function typeAndBrand(Request $req, $slug)
    {
        preg_match('/(.+)-(.+)$/', $slug, $matches);
        if (!$matches) {
            dump($slug);
            dd($matches);
            throw new NotFoundHttpException();
        }
        $bikeType = BicycleType::where('slug', $matches[1])->firstOrFail();
        $brand = Brand::where('slug', $matches[2])->firstOrFail();
        $ads = $this->ad->getAdsByTypeAndBrand($bikeType, $brand);
        $bikeByTypeAndBrand = $this->ad->bikeByTypeAndBrand();
        $title = "Bicicletas ".$bikeType->name.' '.$brand->name." à venda";
        return view("ads.type-brand", compact("ads", "bikeByTypeAndBrand", "bikeType", "brand", "title"));
    }

    public function show(Ad $ad)
    {
        $ad->load("bike", "category", "user.city");
        $related = $this->ad->related($ad);
        $breadCrumb = [[
            "name" => "Inicial",
            "item" => route('home')
        ]];
        $view = view("ads.show", compact("ad", "related"));
        if ($ad->bike) {
            $breadCrumb[] = ["name" => $ad->bike->type->name, "item" => route("ads.bike-type", ["bikeType"=>$ad->bike->type->slug])];
            $breadCrumb[] = ["name" => $ad->brand->name, "item" => route("ads.type-brand", ["slug"=>$ad->bike->type->slug."-".$ad->brand->slug])];
            $view->with([
                "keywords" => "bike ".$ad->bike->type->name." ".$ad->brand->name.", bicicleta ".$ad->bike->type->name." ".$ad->brand->name,
                "title" => "Bicicleta " . $ad->bike->type->name . " " . $ad->description
            ]);
        } else {
            $view->with("title", $ad->description);
        }
        $breadCrumb[] = ["name" => $ad->description];
        if ($ad->photos()->count()) {
            $view->with('socialCover', photo($ad->photos()->first()->file));
        }
        $view->with("breadCrumb", $breadCrumb);
        return $view;
    }

    public function edit(Ad $ad)
    {
        $ad->load("brand");
        return view("ads.form", compact("ad"));
    }

    public function update(EditAd $request, Ad $ad)
    {
        $ad = $this->ad->update($request, $ad);
        return redirect()
            ->route("ads.show", ["ad" => $ad->id, "slug"=>$ad->slug])
            ->with("status", "Anúncio atualizado!");
    }

    public function destroy(DeleteAd $request, Ad $ad)
    {
        $this->ad->destroy($request, $ad);
        return redirect()->route("users.profile", auth()->user())
            ->with("status", "Seu anúncio excluído!");
    }

    public function feed()
    {
        $ads = $this->ad->forFeed();
        $contents = "id	title	description	product type	link	image link	condition	availability	price	brand";
        foreach ($ads as $ad) {
            $line = [
                $ad->id,
                $ad->description,
                preg_replace("/\r\n|\r|\n/", ' ', $ad->details),
                $ad->bike ? "Bicicleta > ".$ad->bike->type->name : $ad->category->name,
                route('ads.show', ['ad'=>$ad->id, "slug" => $ad->slug]),
                $ad->photos->count() ? photo_thumb($ad->photos[0]->file) : '',
                'used',
                'in stock',
                $ad->price . " BRL",
                $ad->brand->name
            ];
            $contents .= "\n".implode("\t", $line);
        }
        // dd($contents);
        Storage::disk('local')->put('feed.txt', $contents);
        return response()->file(Storage::path('feed.txt'));
    }
}
