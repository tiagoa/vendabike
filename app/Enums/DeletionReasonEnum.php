<?php

namespace App\Enums;

class DeletionReasonEnum
{
    public const VAPT = 1;
    public const SOLD = 2;
    public const NOT_SOLD = 3;

    public static $options = [
        self::VAPT => "Vendeu pelo Vapt!",
        self::SOLD => "Vendeu por outro lugar",
        self::NOT_SOLD => "Não vendeu",
    ];
}
