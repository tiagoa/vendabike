@extends('layouts.app')
@section('content')
<div class="flex flex-col px-4 lg:px-0 lg:flex-row">
  <div class="flex flex-col w-full lg:mr-4 lg:w-1/3 bg-white rounded-lg p-8">
    <h2 class="font-bold text-2xl mb-6 text-blue-800">Use sua rede social</h2>
    <a href="{{ route("login.instagram") }}" class="w-full mt-4 rounded-lg bg-pink-600 text-white font-bold text-lg px-4 py-2 hover:bg-pink-800 transition duration-300" onclick="gtag('event', 'social_login', {'event_categoty':'user', 'event_label':'Instagram'})"><i class="icon-instagram mr-3"></i>Instagram</a>
    <a href="{{ route("login.facebook") }}" class="w-full mt-4 rounded-lg bg-blue-600 text-white font-bold text-lg px-4 py-2 hover:bg-blue-800 transition duration-300" onclick="gtag('event', 'social_login', {'event_categoty':'user', 'event_label':'Facebook'})"><i class="icon-facebook mr-3"></i>Facebook</a>
    <a href="{{ route("login.google") }}" class="w-full mt-4 rounded-lg bg-red-600 text-white font-bold text-lg px-4 py-2 hover:bg-red-700 transition duration-300" onclick="gtag('event', 'social_login', {'event_categoty':'user', 'event_label':'Google'})"><i class="icon-google mr-3"></i>Google</a>
  </div>
  <div class="w-full my-8 lg:my-0 lg:w-1/3 lg:mx-4 bg-white rounded-lg p-8 w-full mx-auto">
    <form method="POST" action="{{ route('signIn') }}" onsubit="gtag('event', 'login', {'event_categoty':'user', 'event_label':'Login'})">
      <h2 class="font-bold text-2xl mb-6 text-blue-800">Entrar</h2>
      @csrf
      <div class="mb-4 flex flex-col">
        <label>E-mail</label>
        <input type="text" name="email" class="border-2 rounded-lg py-2 px-4" autocomplete="email">
        @error('email', 'signIn')
          <div class="text-red-500">{{ $message }}</div>
        @enderror
      </div>
      <div class="mb-4 flex flex-col">
        <label>Senha</label>
        <input type="password" name="password" class="border-2 rounded-lg py-2 px-4">
        @error('password', 'signIn')
          <div class="text-red-500">{{ $message }}</div>
        @enderror
      </div>
      <div class="flex flex-col sm:flex-row items-center justify-between">
        <button class="bg-teal-600 hover:bg-teal-700 w-full sm:w-auto text-white font-bold py-2 px-4 rounded-lg">Entrar</button>
        <a href="{{ route("password.request") }}" class="block text-gray-700 text-center mt-4 sm:mt-0 w-full sm:w-auto py-2 px-4 rounded-full hover:text-white hover:bg-blue-500 transition duration-300" onclick="gtag('event', 'forgot_password', {'event_categoty':'user', 'event_label':'Recuperar senha'})">Recuperar senha</a>
      </div>
    </form>
  </div>
  <div class="w-full lg:w-1/3 lg:ml-4 bg-white rounded-lg p-8 w-full mx-auto">
    <form method="POST" action="{{ route('signUp') }}" onsubit="gtag('event', 'signup', {'event_categoty':'user', 'event_label':'Cadastrar'})">
      <h2 class="font-bold text-2xl mb-6 text-blue-800">Cadastre-se</h2>
      <div class="mb-4 flex flex-col">
        <label>E-mail</label>
        <input type="text" name="email" class="border-2 rounded-lg py-2 px-4" autocomplete="email">
        @error('email', 'signUp')
          <div class="text-red-500">{{ $message }}</div>
        @enderror
      </div>
      <div class="mb-4 flex flex-col">
        <label>Senha</label>
        <input type="password" name="password" class="border-2 rounded-lg py-2 px-4">
        @error('password', 'signUp')
          <div class="text-red-500">{{ $message }}</div>
        @enderror
      </div>
      <div class="mb-4 flex flex-col">
        <label>Confirme a senha</label>
        <input type="password" name="password_confirmation" class="border-2 rounded-lg py-2 px-4">
        @error('password_confirmation', 'signUp')
          <div class="text-red-500">{{ $message }}</div>
        @enderror
      </div>
      <div class="flex flex-col sm:flex-row items-center justify-between">
        @csrf
        <button class="bg-teal-600 w-full sm:w-auto hover:bg-teal-700 text-white font-bold py-2 px-4 rounded-lg">Cadastrar</button>
      </div>
    </form>
  </div>
</div>
@endsection
