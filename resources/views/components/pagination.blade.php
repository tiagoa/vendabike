@if ($paginator->hasPages())
<nav role="navigation" aria-label="Pagination Navigation" class="flex flex-col items-center justify-center">
  <div>
    <span class="relative z-0 inline-flex">
      {{-- Previous Page Link --}}
      @if ($paginator->onFirstPage())
      <span aria-disabled="true" aria-label="Anterior" class="relative inline-flex items-center px-4 py-2 text-sm font-medium text-gray-500 bg-white border border-gray-300 cursor-default rounded-l-lg leading-5" aria-hidden="true">
        <i class="icon-chevron-left"></i>
      </span>
      @else
      <a
        href="{{ $paginator->previousPageUrl() }}"
        onclick="gtag('event', 'brand', {'event_categoty':'ad', 'event_label':'pagina anterior'})"
        rel="prev"
        class="relative inline-flex items-center px-4 py-2 text-sm font-medium text-gray-500 bg-white border border-gray-300 rounded-l-lg leading-5 hover:text-gray-400 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-100 active:text-gray-500 transition ease-in-out duration-150"
        aria-label="Anterior"
      >
        <i class="icon-chevron-left"></i>
      </a>
      @endif
      {{-- Pagination Elements --}}
      @foreach ($elements as $element)
      {{-- "Three Dots" Separator --}}
        @if (is_string($element))
          <span aria-disabled="true">
            <span class="relative inline-flex items-center px-4 py-2 -ml-px text-sm font-medium text-gray-700 bg-white border border-gray-300 cursor-default leading-5">{{ $element }}</span>
          </span>
        @endif

        {{-- Array Of Links --}}
        @if (is_array($element))
          @foreach ($element as $page => $url)
            @if ($page == $paginator->currentPage())
              <span aria-current="page">
                <span class="relative inline-flex items-center px-4 py-2 -ml-px text-sm font-medium text-gray-500 bg-white border border-gray-300 cursor-default leading-5">{{ $page }}</span>
              </span>
            @else
              <a
                href="{{ $url }}"
                onclick="gtag('event', 'brand', {'event_categoty':'ad', 'event_label':'pagina {{$page}}'})"
                class="relative inline-flex items-center px-4 py-2 -ml-px text-sm font-medium text-gray-700 bg-white border border-gray-300 leading-5 hover:text-gray-500 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150"
                aria-label="Ir para a página {{ $page }}"
              >
                {{ $page }}
              </a>
            @endif
          @endforeach
        @endif
      @endforeach

      {{-- Next Page Link --}}
      @if ($paginator->hasMorePages())
        <a
          href="{{ $paginator->nextPageUrl() }}"
          onclick="gtag('event', 'brand', {'event_categoty':'ad', 'event_label':'pagina posterior'})"
          rel="next"
          class="relative inline-flex items-center px-4 py-2 -ml-px text-sm font-medium text-gray-500 bg-white border border-gray-300 rounded-r-lg leading-5 hover:text-gray-400 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-100 active:text-gray-500 transition ease-in-out duration-150"
          aria-label="Próxima">
          <i class="icon-chevron-right"></i>
        </a>
      @else
        <span aria-disabled="true" aria-label="Próxima" class="relative inline-flex items-center px-4 py-2 -ml-px text-sm font-medium leading-5 text-gray-500 bg-white border border-gray-300 cursor-default rounded-r-lg leading-5" aria-hidden="true">
          <i class="icon-chevron-right"></i>
        </span>
      @endif
    </span>
  </div>
  <div class="flex items-center mt-2 justify-between">
    <p class="text-sm text-gray-600 leading-5">
      Exibindo
      <span class="font-medium">{{ $paginator->firstItem() }}</span>
      à
      <span class="font-medium">{{ $paginator->lastItem() }}</span>
      de
      <span class="font-medium">{{ $paginator->total() }}</span>
      anúncios
    </p>
  </div>
</nav>
@endif
