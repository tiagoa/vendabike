<?php

namespace App\Http\Controllers\Admin;

use App\Ad;
use App\BicycleType;
use App\Enums\AdStatusEnum;
use App\Enums\DeletionReasonEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\Ad\NewAd;
use App\Http\Requests\Ad\EditAd;
use App\Repositories\AdRepository;
use App\User;
use Illuminate\Http\Request;

class AdsController extends Controller
{
    protected $ad;

    public function __construct(AdRepository $ad)
    {
        $this->ad = $ad;
    }

    public function index()
    {
        $ads = Ad::get();
        $deletionReasonSold = DeletionReasonEnum::SOLD;
        return view("admin.ads.index", compact("ads", "deletionReasonSold"));
    }

    public function create()
    {
        $users = User::get()->pluck("name", "id");
        $status = AdStatusEnum::$name;
        $ad = null;
        return view("admin.ads.form", compact("ad", "status", "users"));
    }

    public function import(Request $request)
    {
        $request->validate([
            'url' => 'required|url',
        ]);
        $ad = $this->ad->import($request->url);
        return redirect()->route("admin.ads.edit", ["ad" => $ad->id]);
    }

    public function store(NewAd $request)
    {
        $ad = $this->ad->store($request);
        return redirect()->route("admin.ads.show", ["ad" => $ad->id]);
    }

    public function bicycleType(BicycleType $bikeType)
    {
        $ads = $this->ad->getAdsByBicycleType($bikeType);
        return view("ads.bike-type", compact("ads", "bikeType"));
    }

    public function show(Ad $ad)
    {
        $ad->load("bike", "user");
        $deletionReasons = DeletionReasonEnum::$options;
        return view("admin.ads.show", compact("ad", "deletionReasons"));
    }

    public function edit(Ad $ad)
    {
        $ad->load("brand", "user");
        $status = AdStatusEnum::$name;
        $users = User::get()->pluck("name", "id");
        return view("admin.ads.form", compact("ad", "status", "users"));
    }

    public function update(EditAd $request, Ad $ad)
    {
        $ad = $this->ad->update($request, $ad);
        return redirect()->route("admin.ads.show", ["ad" => $ad->id]);
    }

    public function destroy(Request $request, Ad $ad)
    {
        $this->ad->destroy($request, $ad);
        return redirect()->route("admin.ads.index");
    }

    public function destroyAll(Request $request)
    {
        $this->ad->destroyAll($request);
        return redirect()->route("admin.ads.index")->with("status", "Anúncios excluidos com sucesso!");
    }
}
