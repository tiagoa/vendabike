<?php

namespace App\Http\Controllers;
use Laravel\Socialite\Facades\Socialite;

class SocialLoginController extends Controller
{
    public function redirectToInstagramProvider()
    {
        return Socialite::driver("instagram")->redirect();
    }

    public function handleInstagramProviderCallback()
    {
        $user = Socialite::driver("instagram")->user();

        dd($user);
    }
}
