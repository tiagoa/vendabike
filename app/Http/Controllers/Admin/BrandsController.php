<?php

namespace App\Http\Controllers\Admin;

use App\Brand;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class BrandsController extends Controller
{
    public function index()
    {
        $brands = Brand::withCount("ads")->get();
        return view("admin.brands.index", compact("brands"));
    }

    public function create()
    {
        $brand = null;
        return view("admin.brands.form", compact("brand"));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $brand = new Brand();
        $brand->name = $request->name;
        $brand->slug = Str::slug($brand->name);
        $brand->save();

        return redirect()->route("admin.brands.index");
    }

    public function edit(Brand $brand)
    {
        return view("admin.brands.form", compact("brand"));
    }

    public function update(Request $request, Brand $brand)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $brand->name = $request->name;
        $brand->slug = Str::slug($brand->name);
        $brand->save();
        return redirect()->route("admin.brands.index");
    }

    public function destroy(Request $request, Brand $brand)
    {
        if ($brand->ads()->withTrashed()->count()) {
            throw ValidationException::withMessages([
                "delete"=>"Marca não pode ser deletada porque possui anúncio vinculado"
            ]);
        }
        $brand->delete();
        return redirect()->route("admin.brands.index")->with("status", "Marca deletada");
    }
}
