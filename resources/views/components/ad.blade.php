<a
  href="{{route('ads.show', ['ad'=>$ad->id, "slug" => $ad->slug])}}"
  title="Ver anúncio"
  class="bg-white flex flex-col h-full rounded shadow-sm transition duration-300 hover:shadow-lg relative"
  itemtype="http://schema.org/Product" itemscope
  onclick="gtag('event', 'show', {'event_categoty':'ad', 'event_label':'{{$ad->description}}'})"
>
  @if ($ad->photos->count())
  <div class="w-full rounded-t bg-no-repeat bg-cover bg-center" style="padding-top:60%;background-image:url('{{ photo_thumb($ad->photos[0]->file) }}')"></div>
  <link itemprop="image" href="{{ photo_thumb($ad->photos[0]->file) }}" />
  @else
  <div class="w-full rounded-t bg-no-repeat bg-cover bg-center bg-gray-100" style="padding-top:60%;background-image:url('img/x.svg')"></div>
  @endif
  <div class="py-3 px-4">
  <div class="flex flex-row items-center justify-between" itemprop="offers" itemtype="http://schema.org/Offer" itemscope>
    <link itemprop="url" href="{{route('ads.show', ['ad'=>$ad->id, "slug" => $ad->slug])}}" />
    <meta itemprop="itemCondition" content="https://schema.org/UsedCondition">
    <meta itemprop="availability" content="https://schema.org/InStock">
    <div class="font-bold text-green-600 text-xl">{{ brl_without_zero($ad->price) }}</div>
    <meta itemprop="priceCurrency" content="BRL" />
    <meta itemprop="price" content="{{ $ad->price }}" />
    <div class="uppercase text-gray-600 font-bold text-xs">
    @if ($ad->bike)
      {{$ad->bike->type->name}}
    @else
      Outros
    @endif
    </div>
  </div>
  @if (isset($ad->brand))
  <div itemprop="brand" itemtype="http://schema.org/Brand" itemscope>
    <meta itemprop="name" content="{{ $ad->brand->name }}">
  </div>
  @endif
  <meta itemprop="description" content="{{ $ad->details }}">
  <div class="font-bold" itemprop="name">{{$ad->description}}</div>
  @if ($ad->receipt || $ad->user)
  <div class="mt-2">
    @if ($ad->receipt)
  <div class="receipt">
    <i class="icon-file-alt text-gray-600"></i> Nota fiscal
  </div>
  @endif
  @if ($ad->city)
  <div>
    <i class="icon-location text-gray-600"></i> {{$ad->city}}
  </div>
  @endif
  </div>
  @endif
  </div>
</a>
