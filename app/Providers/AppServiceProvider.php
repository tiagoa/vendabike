<?php

namespace App\Providers;

use App\BicycleType;
use App\Category;
use App\Photo;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('max_photos', function ($attribute, $value, $parameters, $validator) {
            $actualPhotosQuantity = Photo::where("ad_id", $validator->attributes()["id"])->count();
            $photosToDelete = 0;
            if (isset($validator->attributes()["deletePhotos"])) {
                $photosToDelete = count($validator->attributes()["deletePhotos"]);
            }
            $quantityLeft = $parameters[0] - ($actualPhotosQuantity - $photosToDelete);

            return count($value) <= $quantityLeft;
        }, "Somente :quantity fotos por anúncio");

        Validator::replacer('max_photos', function ($message, $attribute, $rule, $parameters) {
            return str_replace([':quantity'], $parameters[0], $message);
        });
        if (! $this->app->runningInConsole()) {
            $bikeTypes = BicycleType::get();
            $categories = Category::get();
            $keywords = "bikes usadas, bikes novas, bikes, comprar bikes, comprar peças de bikes, acessórios de bikes, componentes de bikes, bicicletas usadas, bicicleta, speed usada, mtb usada, bike seminova, bicicleta seminova, speed seminova, mtb seminova";
            View::share("bikeTypes", $bikeTypes);
            View::share("categories", $categories);
            View::share("keywords", $keywords);
            View::share("title", "Bicicletas novas e usadas à venda");
            View::share("socialCover", "https://vaptbike.com.br/img/social-cover.png");
        }
        Paginator::defaultView('components.pagination');
    }
}
