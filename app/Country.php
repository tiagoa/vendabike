<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $primaryKey = 'cod';
    public $incrementing = false;
    protected $keyType = 'string';
    public $timestamps = false;
}
