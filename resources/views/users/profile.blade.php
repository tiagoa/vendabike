@extends('layouts.app')
@section('content')
<div class="bg-white p-4 mb-8 rounded-lg">
<x-user :user="$user">
  @can('update', $user)
  <div class="flex-1 text-right">
  <a href="{{ route('users.edit-data') }}" class="inline-block  bg-blue-400 hover:bg-blue-500 text-white font-bold py-2 px-4 rounded">Editar dados</a>
  </div>
  @endcan
</x-user>
</div>
@if ($ads->count())
<h2 class="text-2xl font-bold text-gray-500 text-center sm:text-left mb-6">Anúncios de {{$user->name}}</h2>
<x-ad-grid :ads="$ads" />
@else
<div class="bg-blue-200 text-blue-900 text-center rounded-lg p-8">{{$user->name}} ainda não publicou um anúncio.</div>
@endif
@endsection
