<?php

namespace App\Http\Controllers;

use App\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class CitiesController extends Controller
{
    public function getCityByZipCode(Request $request, $zipCode)
    {
        $response = Http::get("https://viacep.com.br/ws/$zipCode/json/");
        $json = $response->json();
        if (!isset($json['siafi'])) {
            return response()->json([], 500);
        }
        $city = City::where('siafi', $json['siafi'])->firstOrFail();
        return response()->json($city);
    }
}
