<?php

namespace App\Repositories;

use App\Ad;
use App\Bicycle;
use App\Brand;
use App\Comment;
use App\Enums\AdStatusEnum;
use App\Enums\ContactTypeEnum;
use App\Enums\DeletionReasonEnum;
use App\Mail\AdPublished;
use App\Photo;
use App\Services\InstagramBot;
use App\User;
use Auth;
use DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Image;

class AdRepository {

    private $perPage = 8;

    public function store($request)
    {
        $ad = new Ad();
        $ad->price = $request->price;
        $ad->source = isset($request->source) ? $request->source : "";
        $ad->description = $request->description;
        $ad->details = $request->details;
        $brand = new Brand();
        if ($request->brand_id == 0) {
            $brand->name = $request->brand;
            $brand->slug = Str::slug($brand->name);
        } else {
            $ad->brand_id = $request->brand_id;
        }
        $ad->usage_time = $request->usage_time;
        if (Auth::guard("admin")->check()) {
            if ($request->user_id) {
                $ad->user_id = $request->user_id;
            } else {
                $newUser = User::create([
                    "name" => $request->instagram,
                    "avatar" => InstagramBot::getProfilePictureFromInstagramByUsername($request->instagram)
                ]);
                $newUser->contacts()->create([
                    "type" => ContactTypeEnum::INSTAGRAM,
                    "value" => $request->instagram
                ]);
                if ($request->whatsapp) {
                    $newUser->contacts()->create([
                        "type" => ContactTypeEnum::WHATSAPP,
                        "value" => $request->whatsapp
                    ]);
                }
                $ad->user_id = $newUser->id;
            }
        } else {
            $ad->user_id = Auth::user()->id;
        }
        $ad->negotiation = $request->negotiation ? 1 : 0;
        $ad->receipt = $request->receipt ? 1 : 0;
        $ad->status = AdStatusEnum::PUBLISHED;
        $ad->category_id = $request->category_id;
        $bike = null;
        if ($ad->category_id == 1) {
            $bike = new Bicycle();
            $bike->bicycle_type_id = $request->bicycle_type_id;
            $bike->frame_size = $request->frame_size;
            $bike->fork_headset = $request->fork_headset;
            $bike->handlebars_stem = $request->handlebars_stem;
            $bike->groupset = $request->groupset;
            $bike->brakes = $request->brakes;
            $bike->pedals = $request->pedals;
            $bike->saddle_seatpost = $request->saddle_seatpost;
            $bike->front_wheel_hub_tire = $request->front_wheel_hub_tire;
            $bike->rear_wheel_hub_tire = $request->rear_wheel_hub_tire;
        }
        DB::transaction(function() use ($ad, $brand, $bike, $request) {
            if ($request->brand_id == 0) {
                $brand->save();
                $ad->brand_id = $brand->id;
            }
            $ad->save();
            if ($bike !== null) {
                $bike->ad_id = $ad->id;
                $bike->save();
            }
            self::uploadPhotos($request, $ad);
        });
        if (!Auth::guard("admin")->check()) {
            Mail::to(env("APP_EMAIL"))->send(new AdPublished($ad));
        }
        return $ad;
    }

    public function update($request, $ad)
    {
        $brand = new Brand();
        if ($request->brand_id == 0) {
            $brand->name = $request->brand;
            $brand->slug = Str::slug($brand->name);
        } else {
            $ad->brand_id = $request->brand_id;
        }
        $ad->price = $request->price;
        $ad->source = isset($request->source) ? $request->source : "";
        $ad->description = $request->description;
        $ad->details = $request->details;
        $ad->usage_time = $request->usage_time;
        $ad->negotiation = $request->negotiation ? 1 : 0;
        $ad->receipt = $request->receipt ? 1 : 0;
        if (Auth::guard("admin")->check()) {
            $ad->status = $request->status;
            if (!$request->user_id) {
                $newUser = User::create([
                    "name" => $request->instagram,
                    "avatar" => InstagramBot::getProfilePictureFromInstagramByUsername($request->instagram)
                ]);
                $newUser->contacts()->create([
                    "type" => ContactTypeEnum::INSTAGRAM,
                    "value" => $request->instagram
                ]);
                if ($request->whatsapp) {
                    $newUser->contacts()->create([
                        "type" => ContactTypeEnum::WHATSAPP,
                        "value" => $request->whatsapp
                    ]);
                }
                $ad->user_id = $newUser->id;
            } else {
                $ad->user_id = $request->user_id;
                if (!empty($request->instagram) || !empty($request->whatsapp)) {
                    $user = User::find($request->user_id);
                    $user->contacts()->delete();
                    if (!empty($request->instagram)) {
                        $user->contacts()->create([
                        "type" => ContactTypeEnum::INSTAGRAM,
                        "value" => $request->instagram
                    ]);
                    }
                    if (!empty($request->whatsapp)) {
                        $user->contacts()->create([
                        "type" => ContactTypeEnum::WHATSAPP,
                        "value" => $request->whatsapp
                    ]);
                    }
                }
            }
        }
        $bike = null;
        if ($request->bike_id) {
            $bike = Bicycle::find($request->bike_id);
            $bike->bicycle_type_id = $request->bicycle_type_id;
            $bike->frame_size = $request->frame_size;
            $bike->fork_headset = $request->fork_headset;
            $bike->handlebars_stem = $request->handlebars_stem;
            $bike->groupset = $request->groupset;
            $bike->brakes = $request->brakes;
            $bike->pedals = $request->pedals;
            $bike->saddle_seatpost = $request->saddle_seatpost;
            $bike->front_wheel_hub_tire = $request->front_wheel_hub_tire;
            $bike->rear_wheel_hub_tire = $request->rear_wheel_hub_tire;
        }
        DB::transaction(function() use ($ad, $brand, $bike, $request) {
            if ($request->brand_id == 0) {
                $brand->save();
                $ad->brand_id = $brand->id;
            }
            $ad->save();
            if ($bike) {
                $bike->save();
            }
            self::uploadPhotos($request, $ad);
            self::deletePhotos($request);
        });
        return $ad;
    }

    public function import($url)
    {
        $ad = new Ad();
        $ad->source = $url;
        $ad->description = "";
        $ad->category_id = 1; // bike
        $ad->user_id = 1;
        $ad->price = 0;
        $ad->status = AdStatusEnum::PENDING;
        $bike = new Bicycle();
        $bike->bicycle_type_id = 1;

        $post = InstagramBot::getPost($url);

        $ad->details = $post->getCaption();
        $ad->save();
        $bike->ad_id = $ad->id;
        $bike->save();

        $path = public_path().DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR."photos".DIRECTORY_SEPARATOR;
        foreach ($post->getSidecarMedias() as $item) {
            if (!$item->getType() != "video") {
                $download = $item->getImageHighResolutionUrl();
                $filename = md5(microtime());
                $webp = Image::make($download)->encode("webp", 90);
                $webp->save($path.$filename.".webp");
                $jpeg = Image::make($download)->encode("jpeg", 90);
                $jpeg->save($path.$filename.".jpeg");

                $photo = new Photo();
                $photo->ad_id = $ad->id;
                $photo->file = $filename;
                $photo->save();

                $thumbWebp = Image::make($path.$filename.".webp");
                $thumbWebp->fit(320, 192);
                $thumbWebp->save($thumbWebp->dirname.DIRECTORY_SEPARATOR.$thumbWebp->filename."t.webp");
                $thumbJpeg = Image::make($path.$filename.".jpeg");
                $thumbJpeg->fit(320, 192);
                $thumbJpeg->save($thumbJpeg->dirname.DIRECTORY_SEPARATOR.$thumbJpeg->filename."t.jpeg");
            }
        }

        return $ad;
    }

    private static function deletePhotos($request)
    {
        if (!isset($request->deletePhotos)) {
            return;
        }
        foreach ($request->deletePhotos as $id) {
            $photo = Photo::find($id);
            $path = $photo->file;
            $thumbPath = photo_thumb_path($path);
            Storage::disk("public")->delete([$path, $thumbPath]);
            $photo->delete();
        }
    }

    private static function uploadPhotos($request, $ad)
    {
        if (!$request->hasfile("photos")) {
            return;
        }
        foreach ($request->file("photos") as $file) {

            $path = public_path().DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR."photos".DIRECTORY_SEPARATOR;
            $filename = md5(microtime());
            $webp = Image::make($file)->encode("webp", 90);
            $webp->save($path.$filename.".webp");
            $jpeg = Image::make($file)->encode("jpeg", 90);
            $jpeg->save($path.$filename.".jpeg");

            $photo = new Photo();
            $photo->ad_id = $ad->id;
            $photo->file = $filename;
            $photo->save();

            $thumbWebp = Image::make($path.$filename.".webp");
            $thumbWebp->fit(320, 192);
            $thumbWebp->save($thumbWebp->dirname.DIRECTORY_SEPARATOR.$thumbWebp->filename."t.webp");
            $thumbJpeg = Image::make($path.$filename.".jpeg");
            $thumbJpeg->fit(320, 192);
            $thumbJpeg->save($thumbJpeg->dirname.DIRECTORY_SEPARATOR.$thumbJpeg->filename."t.jpeg");
        }
    }

    public function related(Ad $ad)
    {
        $priceStart = $ad->price - ($ad->price * 0.15);
        $priceEnd = $ad->price + ($ad->price * 0.15);

        if ($ad->bike) {
            $bikes = Ad::where("id", "!=", $ad->id)
                ->whereBetween("price", [$priceStart, $priceEnd])
                ->orWhereHas("bike", function($q) use ($ad) {
                    $q->where("bicycle_type_id", $ad->bike->bicycle_type_id)
                        ->where("ad_id", "!=", $ad->id)
                        ->orderBy("price", "asc")
                        ->limit(5);
                })
                ->orderBy("price", "asc")
                ->limit(5);

                return $bikes->get();
        }
        $query = Ad::where("id", "!=", $ad->id)
            ->whereBetween("price", [$priceStart, $priceEnd])
            ->orderBy("price", "asc")
            ->limit(5);

            return $query->get();
    }

    public function destroy($request, Ad $ad)
    {
        $ad->load("bike", "comments", "photos");
        foreach ($ad->photos as $photo) {
            $path = $photo->file;
            $thumbPath = photo_thumb_path($path);
            Storage::disk("public")->delete([$path, $thumbPath]);
            $photo->delete();
        }
        if ($ad->bike) {
            $ad->bike->delete();
        }
        if ($ad->comments->count()) {
            Comment::destroy($ad->comments->modelKeys());
        }
        $ad->deletion_reason = $request->sold;
        $ad->status = in_array($request->sold, [DeletionReasonEnum::SOLD, DeletionReasonEnum::VAPT])
            ? AdStatusEnum::SOLD
            : AdStatusEnum::DELETED;
        $ad->save();
        $ad->delete();
    }

    public function destroyAll($request)
    {
        $ads = Ad::whereIn('id', $request->ids)->get();
        foreach ($ads as $ad) {
            $this->destroy($request, $ad);
        }
    }

    public function forHomePage()
    {
        return Ad::with("bike.type", "photos", "user.city")->published()->paginate($this->perPage);
    }

    public function bikeByTypeAndBrand()
    {
        $registries = DB::table("ads")
            ->select("bicycle_types.name as type", "bicycle_types.slug as type_slug", "brands.name as brand", "brands.slug as brand_slug")
            ->join("bicycles", "ads.id", "=", "bicycles.ad_id")
            ->join("bicycle_types", "bicycles.bicycle_type_id", "=", "bicycle_types.id")
            ->join("brands", "ads.brand_id", "=", "brands.id")
            ->where("ads.status", AdStatusEnum::PUBLISHED)
            ->groupBy("type", "type_slug", "brand", "brand_slug")
            ->get();
        return $registries->groupBy("type");
    }

    public function forFeed()
    {
        return Ad::with("bike.type", "photos")->published()->get();
    }

    public function getAdsByBicycleType($bikeType)
    {
        return Ad::with("bike.type", "photos")->published()
            ->whereHas("bike", function ($q) use ($bikeType) {
                $q->where("bicycle_type_id", $bikeType->id);
            })
            ->paginate($this->perPage);
    }

    public function getAdsByBrand($brand)
    {
        return Ad::with("brand", "photos")->published()
            ->whereHas("bike")
            ->where("brand_id", $brand->id)
            ->paginate($this->perPage);
    }

    public function getAdsByTypeAndBrand($bikeType, $brand)
    {
        return Ad::with("brand", "photos")->published()
            ->whereHas("bike", function ($q) use ($bikeType) {
                $q->where("bicycle_type_id", $bikeType->id);
            })
            ->where("brand_id", $brand->id)
            ->paginate($this->perPage);
    }

    public function getAdsByUser($userId)
    {
        return Ad::where("user_id", $userId)->published()
            ->paginate($this->perPage);
    }
}
