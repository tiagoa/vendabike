@extends("layouts.app")
@section("content")
<div class="flex flex-row">
  <div class="bg-white px-8 pt-6 pb-8 w-4/5">
    @yield('content')
  </div>
  <div class="w-1/5">menu</div>
</div>
@endsection()
