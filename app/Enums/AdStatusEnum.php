<?php

namespace App\Enums;

class AdStatusEnum
{
    public const PENDING = 0;
    public const PUBLISHED = 1;
    public const SOLD = 2;
    public const DELETED = 3;
    public static $name = [
        self::PENDING => 'Pendente',
        self::PUBLISHED => 'Publicado',
        self::SOLD => 'Vendido',
        self::DELETED => 'Excluido',
    ];
}
