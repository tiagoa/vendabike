@extends('layouts.app')
@section('content')
<div class="w-full flex flex-col items-center lg:w-1/2 bg-white rounded-lg p-8 mx-auto shadow-xl">
  <div class="bg-green-100 w-full text-green-800 border-green-600 rounded-lg transition-all mx-4 sm:mx-0 p-4 mb-8 border-2">
    <strong>Sua mensagem foi enviada!</strong><br>Em breve você receberá a resposta no e-mail informado.
  </div>
  <a
    href="/"
    class="bg-blue-400 w-full sm:w-1/2 text-center hover:bg-blue-500 text-white font-bold py-2 px-2 sm:px-4 rounded-lg"
    title="Ir para a página inicial"
    >Ir para à página inicial</a>
</div>
@endsection
