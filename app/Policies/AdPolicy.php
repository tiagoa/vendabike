<?php

namespace App\Policies;

use App\Ad;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;

class AdPolicy
{
    use HandlesAuthorization;

    private function checkSameAuthor(Authenticatable $user, Ad $ad)
    {
        if (Auth::guard("admin")->check()) {
            return true;
        }
        return $user->id == $ad->user_id;
    }

    public function update(Authenticatable $user, Ad $ad)
    {
        return $this->checkSameAuthor($user, $ad);
    }

    public function delete(Authenticatable $user, Ad $ad)
    {
        return $this->checkSameAuthor($user, $ad);
    }
}
