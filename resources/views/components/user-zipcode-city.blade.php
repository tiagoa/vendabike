<div class="flex flex-col sm:flex-row mb-4" x-data="initSearch()">
  <div class="flex flex-col sm:w-1/2">
  {!! Form::label("zipcode", "CEP da sua cidade *") !!}
  {!! Form::hidden("city_id", null, ["x-model" => "city_id"]) !!}
  {!! Form::number("zipcode", null, ["class" => "border-2 rounded py-2 px-4", ":disabled"=>"loading", "x-model" => "zipCode", "@input.debounce.250"=>"search()"]) !!}
  </div>
  <div x-show="!loading" class="sm:pl-4 sm:w-1/2 flex items-center pt-6">
    <div x-show="city_id != null">
      <i class="icon-location text-gray-500 mr-2"></i> <span x-text="city_name + ' - ' + uf"></span>
    </div>
    <span x-show="error" style="display:none" class="text-red-500">CEP não encontrado</span>
  </div>
</div>
<script type="text/javascript">
function initSearch() {
  return {
    zipCode: '{{ $user->zipcode }}',
    loading: false,
    city_id: {{ $user->city_id ? $user->city_id : 'null' }},
    error: false,
    city_name: '{{ $user->city ? $user->city->name : '' }}',
    uf: '{{ $user->city ? $user->city->state_uf : '' }}',
    search: function() {
      console.log(this.zipCode.length);
      if (!this.zipCode || this.zipCode.length < 8) {
        return;
      }

      this.loading = true;
      this.error = false;

      const url = document.getElementsByTagName('base')[0].getAttribute('href')+'/cidade/cep/'+this.zipCode;

      fetch(url)
        .then((response) => response.json())
        .then((data) => {
          this.city_id = null;
          this.city_name = null;
          this.uf = null;

          if (!data.hasOwnProperty('id')) {
            this.error = true;
            this.loading = false;
            return;
          }

          this.city_id = data.id;
          this.city_name = data.name;
          this.uf = data.state_uf;

          this.loading = false;
        }).catch(e => {
          console.log('eeeerr',e);
          this.loading = false;
        });
    }
  };
}
</script>
