<!DOCTYPE html>
<html lang="pt-br">
  <head>
    @if (env('APP_URL') != 'http://localhost:8000')
    <!-- Matomo -->
    <script type="text/javascript">
      var _paq = window._paq = window._paq || [];
      /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
      _paq.push(['trackPageView']);
      _paq.push(['enableLinkTracking']);
      (function() {
        var u="https://vaptbike.matomo.cloud/";
        _paq.push(['setTrackerUrl', u+'matomo.php']);
        _paq.push(['setSiteId', '1']);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.type='text/javascript'; g.async=true; g.src='//cdn.matomo.cloud/vaptbike.matomo.cloud/matomo.js'; s.parentNode.insertBefore(g,s);
      })();
    </script>
    <!-- End Matomo Code -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-178144560-1"></script>
    <script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', 'UA-178144560-1');</script>
    <link rel="preconnect" href="https://www.google-analytics.com">
    <link rel="preconnect" href="https://cdn.matomo.cloud">
    <title>{{$title}} - Vapt!</title>
    @else
    <title>Sabium Lancador Web</title>
    @endif
    <meta charset="utf-8">
    <meta name="title" content="Vapt! - Bicicletas novas e usadas à venda">
    <meta name="description" content="Anuncie sua bike gratuitamente e venda num Vapt!">
    <meta name="keywords" content="{{ $keywords }}">
    <meta name="robots" content="index,follow">
    <meta property="og:site_name" content="Vapt!">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ url()->current() }}">
    <meta property="og:title" content="{{$title}} - Vapt!">
    <meta property="og:description" content="Anuncie sua bike gratuitamente e venda num Vapt!">
    <meta property="og:image" content="{{$socialCover}}">
    <meta name="twitter:card" content="summary">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="{{ route('home') }}">
    <link rel="canonical" href="{{ url()->current() }}" />
    <link href="{{ asset("css/main.css") }}" rel="stylesheet">
    <script src="{{ asset("js/manifest.js") }}" defer></script>
    <script src="{{ asset("js/vendor.js") }}" defer></script>
    <script src="{{ asset("js/app.js") }}" defer></script>
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon-16x16.png') }}">
    <meta name="msapplication-TileColor" content="#2a4365">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#2a4365">
  </head>
  <body class="bg-blue-100">
    <div class="w-full sticky top-0 z-10">
      <div class="w-full bg-blue-900 px-2 sm:px-4">
        <div class="container mx-auto flex flex-row items-center justify-between">
          <a href="/" title="Ir para a página inicial" id="logo" class="text-teal-300 text-2xl font-bold"
          ><img src="/img/logo.svg" alt="Vapt!" width="77" height="30"></a>
          <div class="flex flex-row py-2 sm:py-4 items-start">
            @if(auth()->check())
            <div x-data="{open: false}" @mouseenter="open = true" @mouseleave="open = false" class="mr-4 flex flex-row items-center relative z-10">
              <div class="cursor-pointer hidden sm:block mr-4 whitespace-no-wrap text-blue-200">Olá {{auth()->user()->name}}!</div>
              @if (auth()->user()->avatar)
              <picture>
                <source srcset="{{ auth()->user()->avatar }}.webp" type="image/webp">
                <source srcset="{{ auth()->user()->avatar }}.jpeg" type="image/jpeg">
                <img src="{{ auth()->user()->avatar }}.webp" class="w-10 h-10 cursor-pointer rounded-full" alt="Sua foto de perfil">
              </picture>
              @else
                <img src="{{ gravatar(auth()->user()->email) }}" class="w-10 h-10 cursor-pointer rounded-full" alt="Sua foto de perfil">
              @endif
              <div x-cloak x-show.transition="open" style="display:none; top:40px" class="absolute z-10 left-0 sm:right-0 pt-2 bg-white shadow-lg rounded-md overflow-hidden">
                <nav class="flex flex-col text-gray-600 py-2 whitespace-no-wrap">
                  <a href="{{ route("users.edit-data") }}" class="w-full px-6 py-2 transition duration-150 hover:text-indigo-600 hover:underline hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-indigo-600 focus:underline">Editar dados</a>
                  <a href="{{ route('users.profile', ["user" => auth()->user()]) }}" class="w-full px-6 py-2 transition duration-150 hover:text-indigo-600 hover:underline hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-indigo-600 focus:underline">Perfil</a>
                  <a href="{{ route('logout') }}" class="w-full px-6 py-2 transition duration-150 hover:text-indigo-600 hover:underline hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-indigo-600 focus:underline">Sair</a>
                </nav>
              </div>
            </div>
            @else
            <a href="{{ route('getIn') }}" class="text-blue-300 whitespace-no-wrap rounded-lg hover:bg-blue-700 hover:text-blue-100 font-bold py-2 px-2 sm:px-4 mr-2 sm:mr-4" onclick="gtag('event', 'get_in', {'event_categoty':'user', 'event_label':'Entrar'})"><i class="icon-user mr-1 sm:mr-2 text-blue-400"></i>Entrar</a>
            @endif
            <a href="{{ route('ads.create') }}" class="block whitespace-no-wrap bg-blue-400 hover:bg-blue-500 text-white font-bold py-2 px-2 sm:px-4 rounded-lg" onclick="gtag('event', 'new', {'event_categoty':'ad', 'event_label':'Anuncie grátis'})"><i class="icon-megaphone mr-1 sm:mr-2 text-blue-100"></i>Anuncie grátis!</a>
          </div>
        </div>
      </div>
      <div class="w-full bg-blue-100 mb-8 shadow-md">
        <nav class="container mx-auto flex flex-row items-center overflow-x-auto">
          <a href="/" class="text-blue-900 whitespace-no-wrap px-3 sm:px-4 pt-3 sm:pt-4 pb-2 sm:pb-3 border-b-4 hover:text-blue-800 hover:border-blue-800 transition duration-300 {{Route::current()->getName() == 'home' ? 'border-blue-800' : 'border-transparent' }}"><i class="icon-home"></i></a>
          @foreach ($bikeTypes as $bikeType)
            <a href="{{ route("ads.bike-type", ["bikeType" => $bikeType->slug]) }}" class="text-blue-900 whitespace-no-wrap px-3 sm:px-4 pt-3 sm:pt-4 pb-2 sm:pb-3 hover:text-blue-800 hover:border-blue-800 transition duration-300 border-b-4 border-{{Route::current()->getName() == "ads.bike-type" && Route::current()->originalParameter("bikeType") == $bikeType->slug ? "blue-800" : "transparent"}}">{{ $bikeType->name }}</a>
          @endforeach
        </nav>
      </div>
    </div>
    @if (Route::current()->getName() == 'home')
    <div class="w-full text-white -mt-8 mb-8 flex flex-col text-center py-6 sm:py-12 bg-no-repeat bg-cover bg-center" style="background-image:url(/img/cover.jpeg)">
      <div class="text-2xl font-bold">Boas vindas!</div>
      <div class="text-xl">Aqui você vai encontrar sua próxima bike!</div>
    </div>
    @endif
    <div class="w-full sm:px-4">
      <div class="container mx-auto">
        @if ($errors->any())
        <div class="flex flex-col bg-red-100 mb-8 border-2 text-red-800 border-red-500 px-4 pt-4 rounded-lg">
          @foreach ($errors->all() as $error)
            <div class="mb-4">{{ $error }}</div>
          @endforeach
        </div>
        @endif
        @if (session('status'))
          <div class="flex relative bg-green-100 text-green-800 border-green-600 rounded-lg transition-all mx-4 sm:mx-0 duration-300" :class="{'h-0 overflow-hidden p-0':open==false, 'p-4 pr-10 mb-8 border-2':open==true}" x-data="{open:true}">
          {{ session('status') }}
          <i class="icon-cancel absolute right-0 top-0 m-4" @click="open=false"></i>
        </div>
        @endif
        @yield('content')
      </div>
    </div>
    <div class="w-full mt-8 sm:mt-16 py-6 px-4 bg-blue-900 text-blue-100">
      <div class="container mx-auto flex flex-col sm:flex-row sm:justify-between">
        <div class="flex flex-row sm:flex-col justify-between sm:justify-start items-center sm:items-start">
          <a href="/" title="Ir para a página inicial"><img src="/img/logo.png" alt="logo"></a>
          <div class="sm:mt-4">
          <a href="https://facebook.com/vapt.vendeu" target="_blank" rel="noopener" title="Curta Vapt! no Facebook" class="text-blue-300 hover:text-white transition duration-300"><i class="icon-facebook text-2xl"></i></a>
          <a href="https://instagram.com/vapt.vendeu" target="_blank" rel="noopener" title="Siga Vapt! no Instagram" class="text-blue-300 hover:text-white transition duration-300"><i class="icon-instagram text-2xl"></i></a>
          </div>
          <a href="{{ route("contactForm") }}" class="text-blue-300 hover:text-white sm:mt-4">Entre em contato</a>
        </div>
        <div class="flex flex-col justify-between">
          <nav class="flex flex-col mt-4 sm:text-right sm:mt-0">
            <a href="/" title="Ir para a página inicial" class="text-blue-300 py-3 sm:py-0 sm:ml-4 whitespace-no-wrap hover:text-white transition duration-300">Todos os anúncios</a>
            @foreach ($bikeTypes as $bikeType)
            <a href="{{ route("ads.bike-type", ["bikeType" => $bikeType->slug]) }}" class="text-blue-300 py-3 sm:py-0 sm:ml-4 whitespace-no-wrap  hover:text-white transition duration-300" onclick="gtag('event', 'by_type', {'event_categoty':'ad', 'event_label':'{{ $bikeType->name }}'})">Bicicletas {{ $bikeType->name }}</a>
            @endforeach
          </nav>
        </div>
      </div>
    </div>
  </body>
</html>
