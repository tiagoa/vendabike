@extends('layouts.app')
@section('content')
<div class="w-full lg:w-1/2 bg-white rounded-lg p-8 mx-auto shadow-xl">
{!! Form::open(['route' => 'contactForm']) !!}
  @if(auth()->check())
    <h2 class="font-bold text-2xl mb-6 text-blue-800">Escreva sua mensagem {{ auth()->user()->name }}!</h2>
    {!! Form::hidden('user_id', auth()->user()->id) !!}
    <div class="mb-4 flex flex-col">
    {!! Form::textarea('message', null, ["class"=>"border-2 rounded-lg py-2 px-4"]) !!}
    </div>
  @else
    <h2 class="font-bold text-2xl mb-6 text-blue-800">Entre em contato!</h2>
    <div class="mb-4 flex flex-col">
    {!! Form::label('email', 'E-mail') !!}
    {!! Form::text('email', null, ["class"=>"border-2 rounded-lg py-2 px-4"]) !!}
    </div>
    <div class="mb-4 flex flex-col">
    {!! Form::label('name', 'Nome') !!}
    {!! Form::text('name', null, ["class"=>"border-2 rounded-lg py-2 px-4"]) !!}
    </div>
    <div class="mb-4 flex flex-col">
    {!! Form::label('message', 'Mensagem') !!}
    {!! Form::textarea('message', null, ["class"=>"border-2 rounded-lg py-2 px-4"]) !!}
    </div>
  @endif
  <button class="bg-teal-600 hover:bg-teal-700 text-white font-bold py-2 px-4 rounded-lg">Enviar mensagem</button>
{!! Form::close() !!}
</div>
@endsection
