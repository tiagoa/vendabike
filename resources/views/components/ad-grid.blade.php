<div class="flex flex-row justify-center sm:justify-start flex-wrap sm:-mx-4">
  @foreach($ads as $ad)
  <div class="flex-none w-full max-w-sm sm:w-1/2 md:w-1/3 lg:w-1/4 px-4 pt-0 pb-8">
    <x-ad :ad="$ad" />
  </div>
  @endforeach
</div>
<div class="px-4 sm:-mx-4">{{ $ads->onEachSide(2)->links() }}</div>
