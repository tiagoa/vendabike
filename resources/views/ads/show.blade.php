@extends('layouts.app')
@section('content')
<div class="flex flex-col -mt-8 sm:mt-0 md:flex-row">
  <div class="w-full md:w-3/4 md:pr-8">
    @if ($ad->photos->count())
    <div x-data="{ total: {{$ad->photos->count()}}, current: 0, src:''}" >
      <div x-show="src !== ''" class="fixed flex justify-center items-center inset-0 bg-black bg-opacity-75 z-50" @click="src = ''">
        <img x-bind:src="src">
      </div>
      <div class="flex flex-row justify-center bg-gray-900 relative">
        <div
          class="absolute inset-y-0 w-1/4 p-8 flex items-center cursor-pointer text-6xl text-white opacity-50 hover:opacity-100 transition duration-300 left-0"
          @click="if(current >= 1){ current = current - 1} else { current = total-1}"
        >
          <i class="icon-chevron-left"></i>
        </div>
        <div
          class="absolute inset-y-0 w-1/4 p-8 flex items-center justify-end cursor-pointer text-6xl text-white opacity-50 hover:opacity-100 transition duration-300 right-0"
          @click="if(current < total-1){ current = current + 1} else {current = 0}"
        >
          <i class="icon-chevron-right"></i>
        </div>
        @foreach ($ad->photos as $photo)
        <div
          x-show="current == {{$loop->index}}"
          class="cursor-pointer w-full bg-no-repeat bg-cover bg-center"
          style="padding-top:66%; background-image:url('{{photo($photo->file)}}')"
          @click="src = '{{photo($photo->file)}}'"
        ></div>
        @endforeach()
      </div>
      <div class="flex flex-row justify-center bg-gray-800">
      @foreach ($ad->photos as $photo)
        <div
          class="w-1/6 bg-cover bg-center bg-no-repeat cursor-pointer transition duration-300 hover:opacity-75"
          :class="{'current border-4 border-blue-300':current == {{$loop->index}}, 'opacity-50':current != {{$loop->index}} }"
          @click="current = {{$loop->index}}"
          style="padding-top:10%; background-image:url('{{photo_thumb($photo->file)}}')"
        >
        </div>
      @endforeach()
      </div>
    </div>
    @endif
    <div class="bg-white p-4 pb-0">
      <div>
        <div class="float-right text-right ml-2 mr-1">
          @if ($ad->city)
          <div class="mb-2">
            <i class="icon-location text-gray-600"></i> {{$ad->city}}
          </div>
          @endif
          @if ($ad->receipt)
          <div class="mb-2">
            <div class="receipt">
            <i class="icon-file-alt text-gray-600"></i> Nota fiscal
            </div>
          </div>
          @endif
          @if ($ad->negotiation)
          <div class="receipt">
            <i class="icon-handshake text-gray-600"></i> Aceita negociar
          </div>
          @endif
        </div>
        <h2 class="font-bold text-2xl pb-2 text-green-600">{{ brl_without_zero($ad->price) }}</h2>
        <h1 class="font-bold text-2xl pb-3">{{ $ad->description }}</h1>
      </div>
      <div class="pb-4">{!! nl2br($ad->details, 0) !!}</div>
      <div class="flex flex-row -mx-4 border-gray-200 border-t">
        <div class="text-gray-700 font-bold pl-4 pt-4 sm:py-4 sm:w-1/3 mr-2">Marca</div>
        <div>
          <a href="{{ route('ads.brand', ['brand'=>$ad->brand->slug]) }}"
            onclick="gtag('event', 'brand', {'event_categoty':'ad', 'event_label':{{ $ad->brand->name }}})"
            title="Ver mais bicicletas da marca {{$ad->brand->name}}"
            class="flex flex-row m-2 text-blue-500 w-auto items-center border-white border bg-white bg-white rounded-lg hover:border-blue-500 hover:shadow-lg transition duration-300"
          >
            <span
              class="w-4 h-4 ml-2 bg-no-repeat bg-center bg-contain"
              style="background-image:url('/img/brands/{{$ad->brand->slug}}.png')"
            ></span>
            <span class="p-2">
            {{$ad->brand->name}}
            </span>
          </a>
        </div>
      </div>
      @if($ad->bike)
      @if ($ad->bike->frame_size)
      <div class="flex flex-col sm:flex-row -mx-4 p-4 border-gray-200 border-t">
        <div class="text-gray-700 font-bold w-full sm:w-1/3 mr-2">Quadro / Tamanho</div><div>{{$ad->bike->frame_size}}</div>
      </div>
      @endif
      @if ($ad->bike->fork_headset)
      <div class="flex flex-col sm:flex-row -mx-4 p-4 border-gray-200 border-t">
        <div class="text-gray-700 font-bold w-full sm:w-1/3 mr-2">Garfo / Headset</div><div>{{$ad->bike->fork_headset}}</div>
      </div>
      @endif
      @if ($ad->bike->handlebars_stem)
      <div class="flex flex-col sm:flex-row -mx-4 p-4 border-gray-200 border-t">
        <div class="text-gray-700 font-bold w-full sm:w-1/3 mr-2">Guidão / Avanço</div><div>{{$ad->bike->handlebars_stem}}</div>
      </div>
      @endif
      @if ($ad->bike->groupset)
      <div class="flex flex-col sm:flex-row -mx-4 p-4 border-gray-200 border-t">
        <div class="text-gray-700 font-bold w-full sm:w-1/3 mr-2">Grupo</div><div>{{$ad->bike->groupset}}</div>
      </div>
      @endif
      @if ($ad->bike->brakes)
      <div class="flex flex-col sm:flex-row -mx-4 p-4 border-gray-200 border-t">
        <div class="text-gray-700 font-bold w-full sm:w-1/3 mr-2">Freios</div><div>{{$ad->bike->brakes}}</div>
      </div>
      @endif
      @if ($ad->bike->pedals)
      <div class="flex flex-col sm:flex-row -mx-4 p-4 border-gray-200 border-t">
        <div class="text-gray-700 font-bold w-full sm:w-1/3 mr-2">Pedais</div><div>{{$ad->bike->pedals}}</div>
      </div>
      @endif
      @if ($ad->bike->saddle_seatpost)
      <div class="flex flex-col sm:flex-row -mx-4 p-4 border-gray-200 border-t">
        <div class="text-gray-700 font-bold w-full sm:w-1/3 mr-2">Selim / Canote</div><div>{{$ad->bike->saddle_seatpost}}</div>
      </div>
      @endif
      @if ($ad->bike->front_wheel_hub_tire)
      <div class="flex flex-col sm:flex-row -mx-4 p-4 border-gray-200 border-t">
        <div class="text-gray-700 font-bold w-full sm:w-1/3 mr-2">Roda / Cubo / Pneu dianteiro</div><div>{{$ad->bike->front_wheel_hub_tire}}</div>
      </div>
      @endif
      @if ($ad->bike->rear_wheel_hub_tire)
      <div class="flex flex-col sm:flex-row -mx-4 p-4 border-gray-200 border-t">
        <div class="text-gray-700 font-bold w-full sm:w-1/3 mr-2">Roda / Cubo / Pneu traseiro</div><div>{{$ad->bike->rear_wheel_hub_tire}}</div>
      </div>
      @endif
      @endif
    </div>
    <div class="bg-white border-t border-gray-200 p-4">
      @if(Auth::user() && Auth::user()->can('update', $ad))
      <div class="flex flex-col sm:flex-row">
        <a href="{{ route('ads.edit', ['ad' => $ad->id]) }}" class="inline-block bg-blue-400 hover:bg-blue-500 text-white font-bold py-2 px-4 rounded-lg text-center">Atualizar</a>
        <x-remove-ad :ad="$ad" />
      </div>
      @else
      <div class="font-bold mb-4 text-2xl text-blue-800">Se interessou? Entre em contato com:</div>
      <x-user :user="$ad->user" :ad="$ad" />
      @endif
    </div>
    @if ($ad->source)
    <p class="text-gray-500 px-4 text-sm mt-4">Este anúncio foi importado automaticamente. <a href="{{ $ad->source }}" target="_blank" rel="noopener" class="hover:underline">Clique aqui para ver o anúncio original</a>.</p>
    @endif
  </div>
  <div class="w-full flex flex-col items-center mt-8 md:mt-0 md:w-1/4">
    <h2 class="text-2xl font-bold text-gray-500 mb-6">Anúncios relacionados</h2>
    @foreach ($related as $ad)
      <div class="flex flex-col w-full max-w-sm mb-4">
        <x-ad :ad="$ad" />
      </div>
    @endforeach
  </div>
</div>
<script type="application/ld+json">
{
  "@context": "https://schema.org/",
  "@type": "Product",
  "name": "{{ $ad->description }}",
  "image": [
  @if ($ad->photos->count())
  @foreach ($ad->photos as $photo)
    "{{ photo($photo->file) }}.jpeg"{{$loop->last ? "" : ","}}
  @endforeach
  @endif
  ],
  "description": "{{ $ad->details }}",
  "brand": {
    "@type": "Brand",
    "name": "{{ $ad->brand->name }}"
  },
  "offers": {
    "@type": "Offer",
    "url": "{{ route('ads.show', ['ad'=>$ad->id, "slug" => $ad->slug]) }}",
    "priceCurrency": "BRL",
    "price": "{{ $ad->price }}",
    "itemCondition": "https://schema.org/UsedCondition",
    "availability": "https://schema.org/InStock"
  }
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [
    @foreach($breadCrumb as $item)
    {
      "@type": "ListItem",
      "position": {{$loop->index+1}},
      "name": "{{ $item["name"] }}"
      @if (isset($item["item"]))
      ,"item": "{{ $item["item"] }}"
      @endif
    }@if (!$loop->last) , @endif
    @endforeach
  ]
}
</script>
@endsection
