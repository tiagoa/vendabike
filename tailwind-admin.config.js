module.exports = {
  purge: {
    mode: 'all',
    content: [
        "resources/views/admin/**/*.php",
        "resources/views/layouts/admin.blade.php"
    ],
  },
  theme: {
    extend: {},
  },
  variants: {},
  plugins: [],
}
