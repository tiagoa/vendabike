@extends('layouts.admin')
@section('content')
<div class="bg-white rounded-lg p-8">
  @if ($user)
  {!! Form::model($user, ['route' => ['admin.users.update', $user->id], "class"=>"px-4 sm:px-0", 'method' => 'put']) !!}
    <h2 class="font-bold text-2xl text-blue-800 mb-4">Atualizar usuário</h2>
    {!! Form::hidden('id', $user->id) !!}
  @else
  {!! Form::open(['route' => 'admin.users.store', "class"=>"px-4 sm:px-0"]) !!}
    <h2 class="font-bold text-2xl text-blue-800 mb-4">Cadastrar usuário</h2>
  @endif
  <div class="mb-8 flex flex-row">
  @if ($user->avatar)
    <img src="{{ $user->avatar }}.webp" alt="Foto de {{$user->name}}" class="bg-gray-100 rounded-full w-20">
  @else
    <img src="{{ gravatar($user->email) }}" alt="Foto de {{$user->name}}" class="bg-gray-100 rounded-full w-20">
  @endif
  </div>
  <div class="mb-8 flex flex-col sm:flex-row">
    <div class="flex flex-col sm:w-1/2 sm:mr-4">
      {!! Form::label('name', 'Nome') !!}
      {!! Form::text('name', null, ['class' => 'border-2 rounded py-2 px-4']) !!}
      @error('name')
        <div class="text-red-500">{{ $message }}</div>
      @enderror
    </div>
    <div class="flex flex-col sm:w-1/2 sm:ml-4">
      {!! Form::label('email', 'E-mail') !!}
      {!! Form::text('email', null, ['class' => 'border-2 rounded py-2 px-4']) !!}
      @error('email')
        <div class="text-red-500">{{ $message }}</div>
      @enderror
    </div>
  </div>
  <x-user-zipcode-city :user="$user" />
  @foreach ($user->contacts as $contact)
  <div class="flex flex-col mb-8">
    {!! Form::label("contacts[".$contact->type."][value]", $contact->name) !!}
    {!! Form::text("contacts[".$contact->type."][value]", null, ["class" => "border-2 rounded py-2 px-4"]) !!}
    {!! Form::hidden("contacts[".$contact->type."][type]", null) !!}
    {!! Form::hidden("contacts[".$contact->type."][id]", null) !!}
    @if ($contact->type == 0)
    <label>{!! Form::checkbox('import_avatar', "1") !!} Importar avatar usando bot do Instagram</label>
    @endif
    @error("user.contacts[".$contact->type."].value")
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </div>
  @endforeach
  <div>
    <button class="bg-teal-600 hover:bg-teal-700 text-white font-bold py-2 px-4 rounded-lg">Salvar</button>
    <a
      href="{{ route("admin.users.index")}}"
      class="hover:underline text-gray-800 font-bold py-2 px-4 text-center"
    >Cancelar</a>
  </div>
{!! Form::close() !!}
</div>
@endsection
