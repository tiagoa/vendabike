<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactForm;
use App\Mail\ContactFormMail;
use Illuminate\Support\Facades\Mail;

class ContactFormController extends Controller
{

    public function index()
    {
        return view("contact-form.form");
    }
    public function submit(ContactForm $request)
    {
        Mail::to(env("APP_EMAIL"))->send(new ContactFormMail($request));
        return redirect()->action("ContactFormController@success");
    }
    public function success()
    {
        return view("contact-form.success");
    }
}
