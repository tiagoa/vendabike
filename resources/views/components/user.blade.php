<div class="flex flex-row items-start">
  <a href="{{ route("users.profile", ["user" => $user->id]) }}" title="Ver perfil" class="hover:shadow-lg transition duration-300">
    @if ($user->avatar)
    <picture>
      <source srcset="{{ $user->avatar }}.webp" type="image/webp">
      <source srcset="{{ $user->avatar }}.jpeg" type="image/jpeg">
      <img src="{{ $user->avatar }}.webp" alt="Foto de {{$user->name}}" class="bg-gray-100 rounded-full w-20">
    </picture>
    @else
      <img src="{{ gravatar($user->email) }}" alt="Foto de {{$user->name}}" class="bg-gray-100 rounded-full w-20">
    @endif
  </a>
  <div class="flex flex-col ml-4">
    <div>
      <a href="{{ route("users.profile", ["user" => $user->id]) }}" title="Ver perfil" class="text-xl font-bold hover:text-blue-500 transition duration-300">{{ $user->name }}</a>
    </div>
    @if ($user->city)
    <span class="mt-3"><i class="icon-location text-gray-600"></i> {{$user->city->name}} - {{$user->city->state_uf}}</span>
    @endif
    @if ($user->contacts->count() || $user->email)
    <div class="mt-3 flex flex-col sm:flex-row">
      @foreach($user->contacts as $contact)
      @if ($contact->value != $user->email)
      <a href="{{$contact->url}}"
        title="{{$contact->name }}"
        class="cursor-pointer mr-6 hover:text-blue-500 transition duration-300"
        target="_blank"
        rel="noopener"
        {!! isset($ad) ? ' onclick="gtag(\'event\', \'contact\', {\'event_categoty\':\'user\', \'event_label\':\'{{$contact->name}}\'})"' : '' !!}
      >
        <i class="icon-{{$contact->slug}} text-gray-600"></i> {{$contact->name}}
      </a>
      @endif
      @endforeach
      @if($user->email)
      <a href="mailto:{{$user->email}}"
        title="E-mail"
        class="cursor-pointer mr-6 hover:text-blue-500 transition duration-300"
        target="_blank"
        rel="noopener"
        {!! isset($ad) ? ' onclick="gtag(\'event\', \'contact\', {\'event_categoty\':\'user\', \'event_label\':\'email\'})"' : '' !!}
      >
        <i class="icon-mail text-gray-600"></i> Email
      </a>
      @endif
    </div>
    @endif
  </div>
  {{ $slot }}
</div>
