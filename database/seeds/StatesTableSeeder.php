<?php

use App\State;
use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('states')->delete();
        $br = 'BRA';
        $items = [
            ['name'=>'Acre', 'uf'=>'AC', 'country_cod'=>$br],
            ['name'=>'Alagoas', 'uf'=>'AL', 'country_cod'=>$br],
            ['name'=>'Amapá', 'uf'=>'AP', 'country_cod'=>$br],
            ['name'=>'Amazonas', 'uf'=>'AM', 'country_cod'=>$br],
            ['name'=>'Bahia', 'uf'=>'BA', 'country_cod'=>$br],
            ['name'=>'Ceará', 'uf'=>'CE', 'country_cod'=>$br],
            ['name'=>'Distrito Federal', 'uf'=>'DF', 'country_cod'=>$br],
            ['name'=>'Espírito Santo', 'uf'=>'ES', 'country_cod'=>$br],
            ['name'=>'Goiás', 'uf'=>'GO', 'country_cod'=>$br],
            ['name'=>'Maranhão', 'uf'=>'MA', 'country_cod'=>$br],
            ['name'=>'Mato Grosso', 'uf'=>'MT', 'country_cod'=>$br],
            ['name'=>'Mato Grosso do Sul', 'uf'=>'MS', 'country_cod'=>$br],
            ['name'=>'Minas Gerais', 'uf'=>'MG', 'country_cod'=>$br],
            ['name'=>'Pará', 'uf'=>'PA', 'country_cod'=>$br],
            ['name'=>'Paraíba', 'uf'=>'PB', 'country_cod'=>$br],
            ['name'=>'Paraná', 'uf'=>'PR', 'country_cod'=>$br],
            ['name'=>'Pernambuco', 'uf'=>'PE', 'country_cod'=>$br],
            ['name'=>'Piauí', 'uf'=>'PI', 'country_cod'=>$br],
            ['name'=>'Rio de Janeiro', 'uf'=>'RJ', 'country_cod'=>$br],
            ['name'=>'Rio Grande do Norte', 'uf'=>'RN', 'country_cod'=>$br],
            ['name'=>'Rio Grande do Sul', 'uf'=>'RS', 'country_cod'=>$br],
            ['name'=>'Rondônia', 'uf'=>'RO', 'country_cod'=>$br],
            ['name'=>'Roraima', 'uf'=>'RR', 'country_cod'=>$br],
            ['name'=>'Santa Catarina', 'uf'=>'SC', 'country_cod'=>$br],
            ['name'=>'São Paulo', 'uf'=>'SP', 'country_cod'=>$br],
            ['name'=>'Sergipe', 'uf'=>'SE', 'country_cod'=>$br],
            ['name'=>'Tocantins', 'uf'=>'TO', 'country_cod'=>$br],
        ];
        foreach ($items as $item) {
            State::create($item);
        }
    }
}
