<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bicycle extends Model
{
    use SoftDeletes;

    protected $with = ["type"];

    public function type()
    {
        return $this->belongsTo('App\BicycleType', 'bicycle_type_id');
    }

    public function ad()
    {
        return $this->belongsTo('App\Ad');
    }
}
