<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactForm extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required_without:user_id|min:2',
            'email' => 'required_without:user_id|email',
            'message' => 'required|min:10',
        ];
    }

    public function messages()
    {
        return [
            'name.required_without' => 'Preencha seu nome',
            'email.required_without' => 'Preencha seu e-mail',
            'message.required' => 'Digite sua mensagem',
        ];
    }
}
