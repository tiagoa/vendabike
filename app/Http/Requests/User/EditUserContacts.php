<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EditUserContacts extends FormRequest
{
    public function authorize()
    {
        return $this->user()->can("update", $this->user());
    }

    public function rules()
    {
        return [

        ];
    }
}
